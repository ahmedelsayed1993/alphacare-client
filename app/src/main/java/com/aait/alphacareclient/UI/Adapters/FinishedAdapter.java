package com.aait.alphacareclient.UI.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.OrderModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.ReSendOrderActivity;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.DialogUtil;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinishedAdapter extends ParentRecyclerAdapter<OrderModel> {
    String token;
    private ProgressDialog mProgressDialog;
    public FinishedAdapter(Context context, List<OrderModel> data, int layoutId,String token) {
        super(context, data, layoutId);
        this.token = token;

    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        FinishedAdapter.ViewHolder holder = new FinishedAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final FinishedAdapter.ViewHolder viewHolder = (FinishedAdapter.ViewHolder) holder;
        final OrderModel addressesModel = data.get(position);
        viewHolder.order.setText(mcontext.getString(R.string.order)+"  "+addressesModel.getOrder_date()+" _ "+addressesModel.getOrder_time());
        viewHolder.price.setText(addressesModel.getOrder_total_price()+mcontext.getResources().getString(R.string.sar));
        viewHolder.address.setText(addressesModel.getOrder_latlng_address());
        viewHolder.name.setText(addressesModel.getProvider_name());
        Glide.with(mcontext).load(addressesModel.getProvider_avatar()).into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
                PopupMenu popupMenu = new PopupMenu(mcontext, viewHolder.more);

                popupMenu.inflate(R.menu.finish_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit:
                                Intent intent = new Intent(mcontext, ReSendOrderActivity.class);
                                intent.putExtra("id",addressesModel.getOrder_id()+"");
                                mcontext.startActivity(intent);
                                return true;
                            case R.id.rate:
                                Dialog dialog = new Dialog(mcontext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.dialog_add_rate);
                                dialog.getWindow().setGravity(Gravity.CENTER);
                                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                EditText comment;
                                RatingBar rating;
                                Button done;
                                comment = dialog.findViewById(R.id.comment);
                                rating = dialog.findViewById(R.id.rating);
                                done = dialog.findViewById(R.id.done);
                                done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (CommonUtil.checkTextError((AppCompatActivity) mcontext, comment, mcontext.getString(R.string.your_comment))) {
                                            return;
                                        } else {
                                            mProgressDialog = DialogUtil.showProgressDialog(mcontext,mcontext.getString(R.string.please_wait),false);
                                            RetroWeb.getClient().create(ServiceApi.class).rate(mLanguagePrefManager.getAppLanguage(), 0, mSharedPrefManager.getUserData().getToken_id(), token, (int) rating.getRating(), comment.getText().toString(), addressesModel.getProvider_id(), null).enqueue(new Callback<BaseResponse>() {
                                                @Override
                                                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                                    mProgressDialog.dismiss();
                                                    if (response.isSuccessful()) {
                                                        if (response.body().getKey().equals("1")) {
                                                            CommonUtil.makeToast(mcontext, response.body().getMsg());
                                                            dialog.dismiss();

                                                        } else {
                                                            CommonUtil.makeToast(mcontext, response.body().getMsg());
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<BaseResponse> call, Throwable t) {
                                                    CommonUtil.handleException(mcontext, t);
                                                    t.printStackTrace();
                                                    mProgressDialog.dismiss();
                                                }
                                            });
                                        }
                                    }
                                });


                                dialog.show();

                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.time)
        TextView order;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.more)
        ImageView more;
        @BindView(R.id.name)
        TextView name;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

