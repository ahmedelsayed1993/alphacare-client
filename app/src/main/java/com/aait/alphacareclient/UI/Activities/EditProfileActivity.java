package com.aait.alphacareclient.UI.Activities;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.UserModel;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.aait.alphacareclient.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;

import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class EditProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.change_password)
    TextView change_password;
    @BindView(R.id.change_pass)
    LinearLayout change_pass;
    int count = 0;
    private String newToken = null;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.id_number)
    EditText id_number;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.old_password)
    EditText old_password;
    @BindView(R.id.new_password)
    EditText new_password;
    @BindView(R.id.confirm_new_password)
    EditText confirm_new_password;
    int gender = 0;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    private String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/images/PNG");
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( EditProfileActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getProfile();
                Log.e("newToken",newToken);

            }
        });
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_data;
    }
    @OnClick(R.id.change_password)
    void onCahnge(){
        count++;
        if (count%2==1){
            change_pass.setVisibility(View.VISIBLE);
        }else {
            change_pass.setVisibility(View.GONE);
            CommonUtil.makeToast(mContext,getString(R.string.change_password));
        }
    }
    private void setData(UserModel userModel){
        title.setText(userModel.getName());
        Glide.with(mContext).load(userModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.logo)).into(avatar);
        name.setText(userModel.getName());
        phone.setText(userModel.getPhone());
        email.setText(userModel.getEmail());
//        id_number.setText(userModel.getNational_id());
        if (userModel.getGender().equals("1")){
            gender = 1;
            male.setChecked(true);
        }else if (userModel.getGender().equals("2")){
            gender = 2;
            female.setChecked(true);
        }

    }
    @OnClick(R.id.male)
    void onMale(){
        gender = 1;
    }
    @OnClick(R.id.female)
    void onFemale(){
        gender = 2;
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getToken_id(),newToken,0).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        setData(response.body().getData());
                        Log.e("user",new Gson().toJson(response.body().getData()));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updateWithoutPassAvatar(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateWithoutPassAvatar(mLanguagePrefManager.getAppLanguage(),
                0,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),
                phone.getText().toString(),email.getText().toString(),gender).enqueue(
                new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getKey().equals("1")){
                                if (response.body().getUser_status().equals("active")){
                                    CommonUtil.makeToast(mContext,response.body().getMsg());
                                    setData(response.body().getData());
                                    mSharedPrefManager.setUserData(response.body().getData());
                                }else if(response.body().getUser_status().equals("non-active")) {
                                    Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                                    intent.putExtra("data",mSharedPrefManager.getUserData());
                                    startActivity(intent);
                                }}else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                }
        );
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        onBackPressed();
    }
    private void updatePass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateWithoutAvatar(mLanguagePrefManager.getAppLanguage(),0,
                mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString(),gender,old_password.getText().toString(),new_password.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getUser_status().equals("active")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        setData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                    }else if(response.body().getUser_status().equals("non-active")) {
                            Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                            intent.putExtra("data",mSharedPrefManager.getUserData());
                            startActivity(intent);
                    }}else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @OnClick(R.id.confirm)
    void onConfirm(){

            if (old_password.getText().toString().trim().isEmpty()&&new_password.getText().toString().trim().isEmpty()&&confirm_new_password.getText().toString().trim().isEmpty()){
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                        checkFormate(phone,getString(R.string.phone_format))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                        !CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                    return;
                }else {
                    if (gender == 0){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_gender));
                    }else {
                        updateWithoutPassAvatar();
                    }
                }
            }else {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                        checkFormate(phone,getString(R.string.phone_format))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                        !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,old_password,mContext.getString(R.string.old_password))||
                        CommonUtil.checkLength(old_password,mContext.getString(R.string.password_length),6)||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
                        CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_password,mContext.getString(R.string.confirm_new_password))){
                    return;
                }else {
                    if (gender == 0){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_gender));
                    }else {
                        if (new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                            updatePass();
                        }else {
                            CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
                        }

                    }
                }

            }

    }

    public static boolean checkFormate(EditText editText,String message){
        if (editText.getText().toString().startsWith("05")&&editText.getText().toString().length()!=10){
            editText.setError(message);
            return true;
        }
        return false;
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }

    @OnClick(R.id.avatar)
    void onAvatar(){
getPickImageWithPermission();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                 returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                 Log.e("image",new Gson().toJson(returnValue));

                ImageBasePath = returnValue.get(0);
               // Log.e("dddd",new Gson().toJson(getPathFromUri(this,Uri.parse(ImageBasePath))));

               // Picasso.with(this).load(Uri.parse(ImageBasePath)).into(avatar);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                avatar.setImageURI(Uri.parse(ImageBasePath));
                if (ImageBasePath!=null) {
                    updateImage(ImageBasePath);
                }
            }
        }
    }

    private void updateImage(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        RequestBody fileBody = RequestBody.create(MediaType.parse("*/*"),ImageFile);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).update(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),newToken,filePart).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                if (response.body().getKey().equals("1")){
                CommonUtil.makeToast(mContext,response.body().getMsg());
                setData(response.body().getData());
                mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);}
        else if ("file".equalsIgnoreCase(uri.getScheme())) {return uri.getPath();}
        return null;
    }
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public static boolean isExternalStorageDocument(Uri uri) {return "com.android.externalstorage.documents".equals(uri.getAuthority());}
    public static boolean isDownloadsDocument(Uri uri) {return "com.android.providers.downloads.documents".equals(uri.getAuthority());}
    public static boolean isMediaDocument(Uri uri) {return "com.android.providers.media.documents".equals(uri.getAuthority());}
    public static boolean isGooglePhotosUri(Uri uri) {return "com.google.android.apps.photos.content".equals(uri.getAuthority());}
}



