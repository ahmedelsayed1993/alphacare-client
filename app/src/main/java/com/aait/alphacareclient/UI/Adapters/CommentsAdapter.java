package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.CommentsModel;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsAdapter extends ParentRecyclerAdapter<CommentsModel> {

    public CommentsAdapter(Context context, List<CommentsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CommentsAdapter.ViewHolder holder = new CommentsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CommentsAdapter.ViewHolder viewHolder = (CommentsAdapter.ViewHolder) holder;
        final CommentsModel addressesModel = data.get(position);
        viewHolder.name.setText(addressesModel.getUser_name());
        viewHolder.comment.setText(addressesModel.getComment());
        Glide.with(mcontext).load(addressesModel.getUser_image()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);
        viewHolder.rating.setRating(Float.parseFloat(addressesModel.getRate()));
        viewHolder.date.setText(addressesModel.getDate());
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.rating)
        RatingBar rating;
        @BindView(R.id.date)
                TextView date;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
