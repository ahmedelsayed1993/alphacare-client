package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.CommentsModel;
import com.aait.alphacareclient.Models.HomeModel;
import com.aait.alphacareclient.Models.ProviderListResponse;
import com.aait.alphacareclient.Models.providerListModel;
import com.aait.alphacareclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends ParentRecyclerAdapter<providerListModel> {
    public HomeAdapter(Context context, List<providerListModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        HomeAdapter.ViewHolder holder = new HomeAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final HomeAdapter.ViewHolder viewHolder = (HomeAdapter.ViewHolder) holder;
        final providerListModel addressesModel = data.get(position);
        viewHolder.name.setText(addressesModel.getName());
        viewHolder.title.setText(addressesModel.getSection());
        Glide.with(mcontext).load(addressesModel.getImage()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);
        viewHolder.rating.setRating(Float.parseFloat(addressesModel.getFinal_rate()));
        if (addressesModel.isFav()){
            viewHolder.fav.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.fav_blue));
        }else {
            viewHolder.fav.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.heart_black));
        }

        viewHolder.fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.rating)
        RatingBar rating;
        @BindView(R.id.fav)
        ImageView fav;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
