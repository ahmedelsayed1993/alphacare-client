package com.aait.alphacareclient.UI.Activities;

import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.alphacareclient.Base.ParentActivity;

import com.aait.alphacareclient.Models.SiteInfoResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.Pereferences.LanguagePrefManager;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;


import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutAppActivity extends ParentActivity{
    String lang;
    @BindView(R.id.terms)

    WebView terms;


    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    LanguagePrefManager languagePrefManager;

    @Override
    protected void initializeComponents() {
        languagePrefManager = new LanguagePrefManager(mContext);
        lang = getIntent().getStringExtra("lang");
        Log.e("lang",lang);
        mLanguagePrefManager.setAppLanguage(lang);

        title.setText(getString(R.string.about_alph));

           getInfo(lang);



    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms;
    }
    private void getInfo(String lang){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getInfo(lang).enqueue(new Callback<SiteInfoResponse>() {
            @Override
            public void onResponse(Call<SiteInfoResponse> call, Response<SiteInfoResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        WebSettings webSettings = terms.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        terms.loadData(response.body().getData().getSite_about(), "text/html", "utf-8");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteInfoResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


}
