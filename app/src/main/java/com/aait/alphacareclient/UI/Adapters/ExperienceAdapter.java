package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.R;

import java.util.List;

import butterknife.BindView;

public class ExperienceAdapter extends ParentRecyclerAdapter<sectionModel> {
    int selectedPosition = 0;
    public ExperienceAdapter(Context context, List<sectionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ExperienceAdapter.ViewHolder holder = new ExperienceAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ExperienceAdapter.ViewHolder viewHolder = (ExperienceAdapter.ViewHolder) holder;
        final sectionModel addressesModel = data.get(position);
        viewHolder.experience.setText(addressesModel.getTitle());
        viewHolder.experience.setChecked(selectedPosition == position);
        viewHolder.experience.setTag(position);
        viewHolder.experience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position);
                selectedPosition =(Integer) viewHolder.experience.getTag();
                notifyDataSetChanged();
            }
        });
    }

    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.experience)
        RadioButton experience;


        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}


