package com.aait.alphacareclient.UI.Activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.R;

import butterknife.BindView;
import butterknife.OnClick;

public class DoneActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id.text)
    TextView text;
    String msg;
    @Override
    protected void initializeComponents() {
        msg = getIntent().getStringExtra("msg");
        text.setText(msg);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_done;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext,MainActivity.class));
        DoneActivity.this.finish();
    }
}
