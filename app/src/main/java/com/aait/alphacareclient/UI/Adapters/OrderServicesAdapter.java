package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class OrderServicesAdapter extends ParentRecyclerAdapter<ServicesModel> {
    public OrderServicesAdapter(Context context, List<ServicesModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        OrderServicesAdapter.ViewHolder holder = new OrderServicesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final OrderServicesAdapter.ViewHolder viewHolder = (OrderServicesAdapter.ViewHolder) holder;
        final ServicesModel addressesModel = data.get(position);
        viewHolder.service_name.setText(addressesModel.getTitle());
        viewHolder.price.setText(addressesModel.getPrice()+mcontext.getResources().getString(R.string.sar));
        Glide.with(mcontext).load(addressesModel.getImage()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);
        viewHolder.add.setVisibility(View.GONE);

    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.service_name)
        TextView service_name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.add)
        ImageView add;
        @BindView(R.id.service)
        LinearLayout service;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
