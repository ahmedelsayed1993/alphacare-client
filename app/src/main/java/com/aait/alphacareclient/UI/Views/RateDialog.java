package com.aait.alphacareclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.Pereferences.LanguagePrefManager;
import com.aait.alphacareclient.Pereferences.SharedPrefManager;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.MainActivity;
import com.aait.alphacareclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateDialog extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    int id;
    @BindView(R.id.comment)
    EditText comment;
    @BindView(R.id.rating)
    RatingBar rating;

    public RateDialog(@NonNull Context context,String token,int id) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.id = id;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_rate);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
    @OnClick(R.id.done)
    void onDone(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,comment,mContext.getString(R.string.your_comment))){
            return;
        }else {
            rate(null);
        }
    }
    private void rate(String order){
        RetroWeb.getClient().create(ServiceApi.class).rate(languagePrefManager.getAppLanguage(),0,sharedPreferences.getUserData().getToken_id(),token,(int) rating.getRating(),comment.getText().toString(),id,order).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        RateDialog.this.dismiss();
                        mContext.startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
}
