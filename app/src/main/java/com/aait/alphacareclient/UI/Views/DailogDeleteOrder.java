package com.aait.alphacareclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.AddressesResponse;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.OrderModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.Pereferences.LanguagePrefManager;
import com.aait.alphacareclient.Pereferences.SharedPrefManager;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.MapDetectLocationActivity;
import com.aait.alphacareclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailogDeleteOrder extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    OrderModel addressesModel;
    public DailogDeleteOrder(@NonNull Context context, String token, OrderModel addressesModel) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.addressesModel = addressesModel;

    }

    @BindView(R.id.address)
    TextView address;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_delete_order);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        address.setText(mContext.getString(R.string.sure_cancel_order)+(addressesModel.getOrder_date()+" _ "+addressesModel.getOrder_time())+" "+mContext.getResources().getString(R.string.with_provider)+" '"+addressesModel.getProvider_name()+"'");


    }

    @OnClick(R.id.save)
    void onSave(){

        RetroWeb.getClient().create(ServiceApi.class).cancelOrder(languagePrefManager.getAppLanguage(), 0, sharedPreferences.getUserData().getToken_id()
                , token, addressesModel.getOrder_id()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                DailogDeleteOrder.this.cancel();
                if (response.isSuccessful()) {
                    if (response.body().getKey().equals("1")) {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                DailogDeleteOrder.this.cancel();

            }
        });

    }
    @OnClick(R.id.cancel)
    void onCancel(){
        DailogDeleteOrder.this.cancel();
    }
}

