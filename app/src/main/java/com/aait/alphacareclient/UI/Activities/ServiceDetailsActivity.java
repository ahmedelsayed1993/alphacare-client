package com.aait.alphacareclient.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.ServiceResponse;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetailsActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.advantages)
    TextView advantages;
    @BindView(R.id.request)
    Button request;
    String id;
    String offer;
    boolean selected;
    String position;
    ServicesModel servicesModel;
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        position = getIntent().getStringExtra("position");
        offer = getIntent().getStringExtra("offer");
        selected = getIntent().getBooleanExtra("selected",false);
        Log.e("sel",selected+""+id);

        if (selected){
            request.setVisibility(View.GONE);
        }else {
            request.setVisibility(View.VISIBLE);
        }
        if (offer.equals("offer")){
            request.setText(getString(R.string.request_offer));
        }else if (offer.equals("service")){
            request.setText(getString(R.string.request_service));
        }
        getService();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_service_details;
    }
    private void getService(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService(mLanguagePrefManager.getAppLanguage(),id).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        servicesModel = response.body().getData();
                        name.setText(response.body().getData().getTitle());
                        price.setText(response.body().getData().getPrice()+mContext.getResources().getString(R.string.sar));
                        details.setText(response.body().getData().getDesc());
                        advantages.setText(response.body().getData().getAdv());
                        Glide.with(mContext).load(response.body().getData().getImage()).apply(new RequestOptions().error(R.mipmap.logo)).into(image);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.request)
    void onRequest(){
        servicesModel.setSelected(true);
        Intent intentData = new Intent();
        intentData.putExtra("id",servicesModel);
        intentData.putExtra("position",position);
        setResult(RESULT_OK, intentData);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        servicesModel.setSelected(false);
        Intent intentData = new Intent();
//        intentData.putExtra("id",servicesModel);
        setResult(RESULT_CANCELED, intentData);
        finish();
    }
}
