package com.aait.alphacareclient.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.UserModel;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.ProviderDetailsActivity;
import com.aait.alphacareclient.UI.Adapters.FavouriteAdapter;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<UserResponse.FavModel> favModels = new ArrayList<>();
    FavouriteAdapter favouriteAdapter;
    @BindView(R.id.reload)
            ImageView reload;
    String  newToken=null;
    public static FavouriteFragment newInstance(String token) {
        Bundle args = new Bundle();
        FavouriteFragment fragment = new FavouriteFragment();
        args.putString("token",token);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_favourite;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        reload.setVisibility(View.GONE);
        title.setText(getString(R.string.favourite));
        menu.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        favouriteAdapter = new FavouriteAdapter(mContext,favModels,R.layout.recycler_providers);
        favouriteAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(favouriteAdapter);

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                      if (mSharedPrefManager.getLoginStatus()) {
                          getHome(newToken);
                      }
                      else {
                          CommonUtil.makeToast(mContext,getString(R.string.login_first));
                      }

            }
        });

              if (mSharedPrefManager.getLoginStatus()) {
                  getHome(newToken);
              }else {
                  CommonUtil.makeToast(mContext,getString(R.string.login_first));
              }
                Log.e("newToken",newToken);





    }
    private void getHome(String token){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getFav(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),token).enqueue(new Callback<UserModel.FavResponse>() {
            @Override
            public void onResponse(Call<UserModel.FavResponse> call, Response<UserModel.FavResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Log.e("home",new Gson().toJson(response.body().getData()));
                        if (response.body().getData().size()!=0){
                            favouriteAdapter.updateAll(response.body().getData());
                        }else {
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel.FavResponse> call, Throwable t) {
                Log.e("fail",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);


            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext, ProviderDetailsActivity.class);
        Log.e("id", String.valueOf(favModels.get(position).getId()));
        intent.putExtra("id",favModels.get(position).getId()+"");
        startActivity(intent);
    }
}
