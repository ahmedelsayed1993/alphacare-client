package com.aait.alphacareclient.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.password)
    EditText password;
    String newToken= null;
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }
    @OnClick(R.id.forgot_pass)
    void onForgotPass(){
        startActivity(new Intent(mContext,ForgotPasswordActivity.class));
    }
    @OnClick(R.id.create_account)
    void onRegister(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }

    @OnClick(R.id.skip)
    void onSkip(){
        mSharedPrefManager.setLoginStatus(false);
        startActivity(new Intent(mContext,MainActivity.class));
        LoginActivity.this.finish();
    }
    @OnClick(R.id.login)
    void onLogin(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone_email))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))){
            return;
        }else {
            Login();
        }
    }
    private void Login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Login(mLanguagePrefManager.getAppLanguage(),phone.getText().toString(),password.getText().toString(),0,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        mSharedPrefManager.setLoginStatus(true);
                        mSharedPrefManager.setUserData(response.body().getData());
                        startActivity(new Intent(mContext,MainActivity.class));
                        LoginActivity.this.finish();
                    }else if (response.body().getKey().equals("2")){
                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("data",response.body().getData());
                        startActivity(intent);
                    }
                    else  {

                        CommonUtil.makeToast(mContext, response.body().getMsg());

                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
