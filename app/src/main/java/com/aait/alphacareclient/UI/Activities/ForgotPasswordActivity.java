package com.aait.alphacareclient.UI.Activities;

import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Views.NewPasswordDailog;
import com.aait.alphacareclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends ParentActivity {
    NewPasswordDailog newPasswordDailog;
    @BindView(R.id.phone)
    EditText phone;
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_forgot_password;
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))){
            return;
        }else {
            forgotPass();
        }
    }
    private void forgotPass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgotPass(mLanguagePrefManager.getAppLanguage(),phone.getText()
        .toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        newPasswordDailog = new NewPasswordDailog(mContext,response.body().getData(),phone.getText().toString());
                        newPasswordDailog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
}
