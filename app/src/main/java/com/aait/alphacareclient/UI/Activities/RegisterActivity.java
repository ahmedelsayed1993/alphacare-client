package com.aait.alphacareclient.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.aait.alphacareclient.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    String ImageBasePath = null;
    String ImageBasePath1 = null;
    @BindView(R.id.id_image)
    ImageView id_image;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.id_number)
    EditText id_number;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_pass)
    EditText confirm_pass;
    int gender = 0;
    String newToken= null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.REGULAR)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
//    ArrayList<String> returnValue1 = new ArrayList<>();
//    Options options1 = Options.init()
//            .setRequestCode(500)                                                 //Request code for activity results
//            .setCount(1)                                                         //Number of images to restict selection count
//            .setFrontfacing(false)                                                //Front Facing camera on start
//            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
//            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
//            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
//            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( RegisterActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });

    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @OnClick(R.id.male)
    void onMale(){
        gender = 1;
    }
    @OnClick(R.id.female)
    void onFemale(){
        gender = 2;
    }
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
    @OnClick(R.id.profile_image)
    void onProfile(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }
//    @OnClick(R.id.id_image)
//    void onIdImage(){
//        getPickImageWithPermission1();
//    }
//    public void getPickImageWithPermission1() {
//        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
//            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
//                CommonUtil.PrintLogE("Permission not granted");
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
//                            REQUEST_IMAGES);
//                }
//            } else {
//                Pix.start(this, options1);
//                CommonUtil.PrintLogE("Permission is granted before");
//            }
//        } else {
//            CommonUtil.PrintLogE("SDK minimum than 23");
//            Pix.start(this, options1);
//        }
//    }
@OnClick(R.id.register)
void onRegister(){
        if (ImageBasePath!=null){

                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_pass,getString(R.string.confirm_password))){
                    return;
                }else {
                    if (!password.getText().toString().equals(confirm_pass.getText().toString())){
                        CommonUtil.makeToast(mContext,getString(R.string.password_not_match));
                    }else {
                        if (gender == 0){
                            CommonUtil.makeToast(mContext,getString(R.string.choose_gender));
                        }else {
                            register(ImageBasePath);
                        }
                    }
                }

        }else {
            if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                    CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_pass,getString(R.string.confirm_password))){
                return;
            }else {
                if (!password.getText().toString().equals(confirm_pass.getText().toString())){
                    CommonUtil.makeToast(mContext,getString(R.string.password_not_match));
                }else {
                    if (gender == 0){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_gender));
                    }else {
                        register();
                    }
                }
            }
        }

}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                 returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
                Log.e("yyyy",ImageBasePath);
               // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                profile_image.setImageURI(Uri.parse(ImageBasePath));
            }
//            else if (requestCode == 500){
//                 returnValue1 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
//
//                ImageBasePath1 = returnValue1.get(0);
//                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
//                id_image.setImageURI(Uri.parse(ImageBasePath1));
//            }
        }
    }

    private void register(String path){
     showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        RequestBody fileBody = RequestBody.create(MediaType.parse("*/*"),ImageFile);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).signUp(mLanguagePrefManager.getAppLanguage(),filePart,name.getText().toString(),
                phone.getText().toString(),email.getText().toString(),gender+"",password.getText().toString(),
                0,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("data",response.body().getData());
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void register(){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;

        RetroWeb.getClient().create(ServiceApi.class).signUpwithout(mLanguagePrefManager.getAppLanguage(),name.getText().toString(),
                phone.getText().toString(),email.getText().toString(),gender+"",password.getText().toString(),
                0,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("data",response.body().getData());
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
