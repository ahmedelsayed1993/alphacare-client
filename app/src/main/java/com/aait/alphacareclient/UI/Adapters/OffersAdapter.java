package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class OffersAdapter extends ParentRecyclerAdapter<ServicesModel> {
    public OffersAdapter(Context context, List<ServicesModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        OffersAdapter.ViewHolder holder = new OffersAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final OffersAdapter.ViewHolder viewHolder = (OffersAdapter.ViewHolder) holder;
        final ServicesModel addressesModel = data.get(position);
        viewHolder.service_name.setText(addressesModel.getTitle());
        viewHolder.price.setText(addressesModel.getPrice());
        Glide.with(mcontext).load(addressesModel.getImage()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);
        viewHolder.offer_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemClickListener.onItemClick(view,position);


            }
        });
        viewHolder.checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
        if (addressesModel.isSelected()){
            viewHolder.checked.setVisibility(View.VISIBLE);
        }else {
            viewHolder.checked.setVisibility(View.GONE);
        }
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.title)
        TextView service_name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.offer_lay)
        RelativeLayout offer_lay;
        @BindView(R.id.checked)
                ImageView checked;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
