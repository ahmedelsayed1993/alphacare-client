package com.aait.alphacareclient.UI.Activities;



import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Gps.GPSTracker;
import com.aait.alphacareclient.Gps.GpsTrakerListener;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.DialogUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.OnClick;

public class SplashActivity extends ParentActivity implements GpsTrakerListener {
    String mResult;
    GoogleMap googleMap;
    Marker myMarker , shopMarker;
    Geocoder geocoder;
    GPSTracker gps;
    boolean startTracker = false;
    public String mLang= "46.738586", mLat="24.774265";
    MarkerOptions markerOptions;
    private AlertDialog mAlertDialog;


    @Override
    protected void initializeComponents() {
        getLocationWithPermission();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocationWithPermission();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));

    }

    @OnClick(R.id.register)
    void onRegister(){
       startActivity(new Intent(mContext,RegisterActivity.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCache(getApplicationContext());
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
               // putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
    }

    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                //  putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                geocoder = new Geocoder(mContext, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(mContext, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult = addresses.get(0).getAddressLine(0);
                        // latest.setText(mResult);
                    }

                } catch (IOException e) {
                }
//                googleMap.clear();
//                putMapMarker(gps.getLatitude(), gps.getLongitude());
                if (mSharedPrefManager.getLoginStatus()) {
                    Log.e("user", new Gson().toJson(mSharedPrefManager.getUserData()));
                    startActivity(new Intent(mContext, MainActivity.class));
                } else {

                }
            }
        }
    }

}
