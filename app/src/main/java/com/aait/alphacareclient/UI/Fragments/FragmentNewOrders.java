package com.aait.alphacareclient.UI.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Listeners.PaginationScrollListener;
import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.OrderModel;
import com.aait.alphacareclient.Models.OrderResponse;
import com.aait.alphacareclient.Models.OrdersResponse;
import com.aait.alphacareclient.Models.QuestionsResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.AddressesActivity;
import com.aait.alphacareclient.UI.Adapters.AddressesAdapter;
import com.aait.alphacareclient.UI.Adapters.OrdersAdapter;
import com.aait.alphacareclient.UI.Adapters.QuestionsAdapter;
import com.aait.alphacareclient.UI.Views.DailogDeleteAddress;
import com.aait.alphacareclient.UI.Views.DailogDeleteOrder;
import com.aait.alphacareclient.UI.Views.DialogEditAddress;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNewOrders extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<OrderModel> orderModels = new ArrayList<>();
    OrdersAdapter ordersAdapter;
    String newToken = null;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected int getLayoutResource() {
        return R.layout.app_recycle;
    }
    public static FragmentNewOrders newInstance(String token) {
        Bundle args = new Bundle();
        FragmentNewOrders fragment = new FragmentNewOrders();
        args.putString("token",token);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                newToken = instanceIdResult.getToken();
//                Log.e("newToken",newToken);
//
//            }
//        });
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        ordersAdapter = new OrdersAdapter(mContext,orderModels,R.layout.recycler_order,newToken);
        ordersAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(ordersAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
//                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
//                    @Override
//                    public void onSuccess(InstanceIdResult instanceIdResult) {
//                        newToken = instanceIdResult.getToken();
                        getQuestions(newToken);
                        Log.e("newToken",newToken);

//                    }
//                });

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                orderModels.clear();
                isLastPage = false;
                isLoading = false;
//                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
//                    @Override
//                    public void onSuccess(InstanceIdResult instanceIdResult) {
//                        newToken = instanceIdResult.getToken();
                        getQuestions(newToken);
                        Log.e("newToken",newToken);

//                    }
//                });
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        orderModels.clear();
        isLastPage = false;
        isLoading = false;
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                newToken = instanceIdResult.getToken();
                getQuestions(newToken);
                Log.e("newToken",newToken);

//            }
//        });
    }

    private void getQuestions(String token){
        if (mSharedPrefManager.getLoginStatus()) {
            layProgress.setVisibility(View.VISIBLE);
            layNoInternet.setVisibility(View.GONE);
            layNoItem.setVisibility(View.GONE);
            RetroWeb.getClient().create(ServiceApi.class).getNew(mLanguagePrefManager.getAppLanguage(), 0, mSharedPrefManager.getUserData().getToken_id(), token, currentPage).enqueue(new Callback<OrdersResponse>() {
                @Override
                public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                    layProgress.setVisibility(View.GONE);
                    swipeRefresh.setRefreshing(false);
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                            if (currentPage == 1 && response.body().getData().getData().isEmpty()) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                            }
                            if (currentPage < TOTAL_PAGES) {
                                if (response.body().getData().getData().isEmpty()) {
                                    ordersAdapter.updateAll(response.body().getData().getData());
                                } else {
                                    ordersAdapter.InsertAll(response.body().getData().getData());
                                }
                                isLoading = false;
                            } else if (currentPage == TOTAL_PAGES) {
                                if (response.body().getData().getData().isEmpty()) {
                                    ordersAdapter.updateAll(response.body().getData().getData());
                                    isLastPage = true;
                                } else {
                                    ordersAdapter.InsertAll(response.body().getData().getData());
                                }
                            } else {
                                isLastPage = true;
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<OrdersResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    layNoInternet.setVisibility(View.VISIBLE);
                    layNoItem.setVisibility(View.GONE);
                    layProgress.setVisibility(View.GONE);
                    swipeRefresh.setRefreshing(false);
                }
            });
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }
    }

    @Override
    public void onItemClick(View view, int position) {


    }
}
