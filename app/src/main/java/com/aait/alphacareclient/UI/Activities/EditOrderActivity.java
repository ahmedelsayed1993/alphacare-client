package com.aait.alphacareclient.UI.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.OrderDetailsModdel;
import com.aait.alphacareclient.Models.OrderDetailsResponse;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Adapters.EditOffersAdapter;
import com.aait.alphacareclient.UI.Adapters.EditServicesAdapter;
import com.aait.alphacareclient.UI.Adapters.OffersAdapter;
import com.aait.alphacareclient.UI.Adapters.ServicesAdapter;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditOrderActivity extends ParentActivity implements OnItemClickListener {
    ArrayList<ServicesModel> servicesModels = new ArrayList<>();
    ArrayList<ServicesModel> offers = new ArrayList<>();
    @BindView(R.id.service)
    RecyclerView service;
    @BindView(R.id.offer)
    RecyclerView offer;
    LinearLayoutManager linearLayoutManager;
    EditOffersAdapter offersAdapter;
    GridLayoutManager gridLayoutManager;
    EditServicesAdapter servicesAdapter;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.rating)
    RatingBar rating;
    int count=0,count1=0;
    String newToken=null;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.day)
    TextView day;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.location)
    TextView location;
    String mAdresse="", mLang=null, mLat = null, mAddress="",Address = "";
    ArrayList<Integer> ids = new ArrayList<>();
    AddressesModel addressesModel;
    String Id = "";
    String ID = "";
    String IDS = "";
    String order;
    OrderDetailsModdel orderDetailsModdel;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.edit_appointment));
        order = getIntent().getStringExtra("id");

        gridLayoutManager = new GridLayoutManager(mContext,2);
        servicesAdapter = new EditServicesAdapter(mContext,servicesModels,R.layout.recycler_service);
        servicesAdapter.setOnItemClickListener(this);
        service.setLayoutManager(gridLayoutManager);
        service.setAdapter(servicesAdapter);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        offersAdapter = new EditOffersAdapter(mContext,offers,R.layout.recycler_offers);
        offersAdapter.setOnItemClickListener(this);
        offer.setLayoutManager(linearLayoutManager);
        offer.setAdapter(offersAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( EditOrderActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getOrder(newToken);
                Log.e("newToken",newToken);

            }
        });


    }

    @OnClick(R.id.services)
    void onServices(){
        count++;
        if (count%2==1) {
            service.setVisibility(View.VISIBLE);
        }else {
            service.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.offers)
    void onOffers(){
        count1++;
        if (count1%2==1) {
            offer.setVisibility(View.VISIBLE);
        }else {
            offer.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.day)
    void onDay(){
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                day.setText(dayOfMonth + "-" + (monthOfYear+1) + "-" + year);
            }};

        DatePickerDialog dialog =
                new DatePickerDialog(this, listener, mYear, mMonth, mDay);
        dialog.show();
    }
    @OnClick(R.id.time)
    void onTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText( selectedHour + "." + selectedMinute);
            }
        }, hour, minute, false);
        //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    @OnClick(R.id.location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    void setData(OrderDetailsModdel orderDetailsModdel){

        day.setText(orderDetailsModdel.getOrder_date());
        time.setText(orderDetailsModdel.getOrder_time());
        Glide.with(mContext).load(orderDetailsModdel.getProvider_avatar()).into(image);
        name.setText(orderDetailsModdel.getProvider_name());
        rating.setRating(Float.parseFloat(orderDetailsModdel.getProvider_final_rate()));
        address.setText(orderDetailsModdel.getProvider_section());
        servicesAdapter.updateAll(orderDetailsModdel.getNormal_services());
        offersAdapter.updateAll(orderDetailsModdel.getOffer_services());
        for (int i=0;i<orderDetailsModdel.getNormal_services().size();i++){
            ids.add(orderDetailsModdel.getNormal_services().get(i).getId());
        }
        Log.e("id",new Gson().toJson(ids));
        for (int i=0;i<orderDetailsModdel.getOffer_services().size();i++){
            ids.add(orderDetailsModdel.getOffer_services().get(i).getId());
        }
        Log.e("idd",new Gson().toJson(ids));
        Id = new Gson().toJson(ids);
        ID = Id.replace("[","");
        IDS = ID.replace("]","");
        Log.e("ID",IDS);
    }
    private void getOrder(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),token,order).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        orderDetailsModdel = response.body().getData();
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_order;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.add){
            servicesModels.remove(servicesModels.get(position));
            ids.clear();
            servicesAdapter.notifyDataSetChanged();

            for (int i=0;i<servicesModels.size();i++){
                ids.add(servicesModels.get(i).getId());
            }
            Log.e("id",new Gson().toJson(ids));
            for (int i=0;i<offers.size();i++){
                ids.add(offers.get(i).getId());
            }
            Log.e("idd",new Gson().toJson(ids));
            Id = new Gson().toJson(ids);
            ID = Id.replace("[","");
            IDS = ID.replace("]","");
            Log.e("ID",IDS);
        }else if (view.getId()==R.id.checked){
            Log.e("of",offers.get(position).getId()+"");
            offers.remove(offers.get(position));
            ids.clear();
            offersAdapter.notifyDataSetChanged();
            for (int i=0;i<servicesModels.size();i++){
                ids.add(servicesModels.get(i).getId());
            }
            Log.e("id",new Gson().toJson(ids));
            for (int i=0;i<offers.size();i++){
                ids.add(offers.get(i).getId());
            }
            Log.e("idd",new Gson().toJson(ids));
            Id = new Gson().toJson(ids);
            ID = Id.replace("[","");
            IDS = ID.replace("]","");
            Log.e("ID",IDS);
        }
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError(location,getString(R.string.location))||
                CommonUtil.checkTextError(day,getString(R.string.day))||
                CommonUtil.checkTextError(time,getString(R.string.time))){
            return;
        }else {
            if (IDS.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.services));
            }else {
               EditOrder();
            }
        }
    }

    private void EditOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).editOrder(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),
                newToken,orderDetailsModdel.getProvider_id(),order,IDS,mLat,mLang,mAddress,mAdresse,day.getText().toString(),time.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        onBackPressed();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    Address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        location.setText(mAddress);
                    } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                        location.setText(mAdresse);
                    }

                }
            }
        }
    }
}
