package com.aait.alphacareclient.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Gps.GPSTracker;
import com.aait.alphacareclient.Gps.GpsTrakerListener;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.OrderDetailsModdel;
import com.aait.alphacareclient.Models.OrderDetailsResponse;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Adapters.OffersAdapter;
import com.aait.alphacareclient.UI.Adapters.OrderServicesAdapter;
import com.aait.alphacareclient.UI.Adapters.ServicesAdapter;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends ParentActivity implements OnMapReadyCallback, GpsTrakerListener, OnItemClickListener {
    Geocoder geocoder;
    GoogleMap googleMap;
    GPSTracker gps;

    public String mLang, mLat;
    boolean startTracker = false;
    private AlertDialog mAlertDialog;
    Marker myMarker;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    String id;
    String newToken = null;
    @BindView(R.id.order)
    TextView order;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.map)
    MapView map;
    ArrayList<ServicesModel> servicesModels = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    OrderServicesAdapter servicesAdapter;
    @BindView(R.id.services)
    RecyclerView services;
    @BindView(R.id.offers)
    RecyclerView offers;
    LinearLayoutManager linearLayoutManager;
    OffersAdapter offersAdapter;
    ArrayList<ServicesModel> offersModel = new ArrayList<>();
    OrderDetailsModdel orderDetailsModdel;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.order_details));
        id = getIntent().getStringExtra("id");
        gridLayoutManager = new GridLayoutManager(mContext,2);
        servicesAdapter = new OrderServicesAdapter(mContext,servicesModels,R.layout.recycler_service);
        services.setLayoutManager(gridLayoutManager);
        services.setAdapter(servicesAdapter);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        offersAdapter = new OffersAdapter(mContext,offersModel,R.layout.recycler_offers);
        offersAdapter.setOnItemClickListener(this);
        offers.setLayoutManager(linearLayoutManager);
        offers.setAdapter(offersAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( OrderDetailsActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getOrder(newToken);
                Log.e("newToken",newToken);

            }
        });
        map.onCreate(mSavedInstanceState);
        map.onResume();
        map.getMapAsync(this);

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_details;
    }
    void setData(OrderDetailsModdel orderDetailsModdel){
        order.setText(getString(R.string.order)+" "+orderDetailsModdel.getOrder_id()+"#");
        price.setText(orderDetailsModdel.getOrder_total_price()+getString(R.string.sar));
        date.setText(getString(R.string.day)+" "+orderDetailsModdel.getOrder_date());
        time.setText(getString(R.string.time)+ " "+orderDetailsModdel.getOrder_time());
        Glide.with(mContext).load(orderDetailsModdel.getProvider_avatar()).into(image);
        name.setText(orderDetailsModdel.getProvider_name());
        rating.setRating(Float.parseFloat(orderDetailsModdel.getProvider_final_rate()));
        address.setText(orderDetailsModdel.getOrder_latlng_address());
        putMapMarker(Double.parseDouble(orderDetailsModdel.getOrder_lat()),Double.parseDouble(orderDetailsModdel.getOrder_lng()));
        servicesAdapter.updateAll(orderDetailsModdel.getNormal_services());
        offersAdapter.updateAll(orderDetailsModdel.getOffer_services());
    }
    private void getOrder(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),token,id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        orderDetailsModdel = response.body().getData();
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                //putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getLocationWithPermission();
    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getApplicationContext(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
//                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
//            getCurrentLocation();
        }

    }
    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_red));

        myMarker = googleMap.addMarker(marker);
    }
    @OnClick(R.id.contact)
    void onContact(){
        Intent intent = new Intent(mContext,ChatActivity.class);
        intent.putExtra("room",orderDetailsModdel.getRoom_id()+"");
        intent.putExtra("token",newToken);
        intent.putExtra("name",orderDetailsModdel.getProvider_name());
        startActivity(intent);
    }


    @Override
    public void onItemClick(View view, int position) {

    }
}
