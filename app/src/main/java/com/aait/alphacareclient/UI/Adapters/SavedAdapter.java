package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.R;

import java.util.List;

import butterknife.BindView;

public class SavedAdapter extends ParentRecyclerAdapter<AddressesModel> {
    public SavedAdapter(Context context, List<AddressesModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        SavedAdapter.ViewHolder holder = new SavedAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final SavedAdapter.ViewHolder viewHolder = (SavedAdapter.ViewHolder) holder;
        final AddressesModel addressesModel = data.get(position);
        viewHolder.provider.setText(addressesModel.getLatlng_address());
        viewHolder.provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.saved_locations)
        TextView provider;


        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
