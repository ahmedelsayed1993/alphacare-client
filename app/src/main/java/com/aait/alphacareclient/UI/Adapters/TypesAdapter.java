package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.ListModel;
import com.aait.alphacareclient.R;


import java.util.List;

import butterknife.BindView;

public class TypesAdapter extends ParentRecyclerAdapter<ListModel> {
    public TypesAdapter(Context context, List<ListModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        TypesAdapter.ViewHolder holder = new TypesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final TypesAdapter.ViewHolder viewHolder = (TypesAdapter.ViewHolder) holder;
        final ListModel categoryModel = data.get(position);
        viewHolder.name.setText(categoryModel.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
