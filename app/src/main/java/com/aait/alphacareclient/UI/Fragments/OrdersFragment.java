package com.aait.alphacareclient.UI.Fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Adapters.SubscribeTapAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;

public class OrdersFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.orders)
    TabLayout myOrdersTab;
    @BindView(R.id.reload)
    ImageView reload;

    @BindView(R.id.ordersViewPager)
    ViewPager myOrdersViewPager;
    String token;
    private SubscribeTapAdapter mAdapter;
    public static OrdersFragment newInstance(String token) {
        Bundle args = new Bundle();
        OrdersFragment fragment = new OrdersFragment();
        args.putString("token",token);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_orders;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        token = bundle.getString("token");
        reload.setVisibility(View.GONE);
        title.setText(getString(R.string.my_orders));
        menu.setVisibility(View.GONE);
        mAdapter = new SubscribeTapAdapter(mContext,getChildFragmentManager(),token);
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);

    }
}
