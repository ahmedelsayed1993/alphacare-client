package com.aait.alphacareclient.UI.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.Models.NotificationCountRespons;
import com.aait.alphacareclient.Models.NotificationsModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.AboutAppActivity;
import com.aait.alphacareclient.UI.Activities.ActivateAccountActivity;
import com.aait.alphacareclient.UI.Activities.ActivityTerms;
import com.aait.alphacareclient.UI.Activities.AddressesActivity;
import com.aait.alphacareclient.UI.Activities.ContactUsActivity;
import com.aait.alphacareclient.UI.Activities.LoginActivity;
import com.aait.alphacareclient.UI.Activities.MainActivity;
import com.aait.alphacareclient.UI.Activities.NotificationActivity;
import com.aait.alphacareclient.UI.Activities.QuestionsActivity;
import com.aait.alphacareclient.UI.Activities.SeetingsActivity;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    private String newToken= null;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.reload)
    ImageView reload;
    public static MoreFragment newInstance(String token) {
        Bundle args = new Bundle();
        MoreFragment fragment = new MoreFragment();
        args.putString("token",token);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_more;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        reload.setVisibility(View.GONE);
        title.setText(getString(R.string.menu));
        menu.setVisibility(View.GONE);

        if (mSharedPrefManager.getLoginStatus()) {
            getCount(newToken);
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }
                Log.e("newToken",newToken);


    }
    @OnClick(R.id.addresses)
    void onMyAddress(){
       if (mSharedPrefManager.getLoginStatus()) {
           startActivity(new Intent(mContext, AddressesActivity.class));
       }else {
           CommonUtil.makeToast(mContext,getString(R.string.login_first));
       }
    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }
    }
    @OnClick(R.id.about_app)
    void onAboutApp(){
       Intent intent = new Intent(mContext,AboutAppActivity.class);
       intent.putExtra("lang",mLanguagePrefManager.getAppLanguage());
        startActivity(intent);
    }
    @OnClick(R.id.repeated_questions)
    void onReoeated(){
         startActivity(new Intent(mContext, QuestionsActivity.class));
    }
    @OnClick(R.id.terms_conditions)
    void onTerms(){
      startActivity(new Intent(mContext, ActivityTerms.class));
    }
    @OnClick(R.id.register_as_provider)
    void onRegister(){
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.alphacareprovider")));
    }
    @OnClick(R.id.contact_us)
    void onContact(){
       startActivity(new Intent(mContext, ContactUsActivity.class));
    }
    @OnClick(R.id.share_app)
    void onShare(){
        CommonUtil.ShareApp(mContext);
    }
    @OnClick(R.id.settings)
    void onSettings(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, SeetingsActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }
    }

    private void getCount(String token){
      //  showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCount(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),token).enqueue(new Callback<NotificationCountRespons>() {
            @Override
            public void onResponse(Call<NotificationCountRespons> call, Response<NotificationCountRespons> response) {
               // hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        count.setText(response.body().getData()+"");
                    }else if (response.body().getKey().equals("2")&&response.body().getUser_status().equals("non-active")){

                        Intent intent = new Intent(mContext, ActivateAccountActivity.class);
                        intent.putExtra("data",mSharedPrefManager.getUserData());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationCountRespons> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
               // hideProgressDialog();

            }
        });
    }

}
