package com.aait.alphacareclient.UI.Fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.Gps.GPSTracker;
import com.aait.alphacareclient.Gps.GpsTrakerListener;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.AddressesResponse;
import com.aait.alphacareclient.Models.HomeModel;
import com.aait.alphacareclient.Models.HomeResponse;
import com.aait.alphacareclient.Models.SectionResponse;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.ActivateAccountActivity;
import com.aait.alphacareclient.UI.Activities.MainActivity;
import com.aait.alphacareclient.UI.Activities.ProviderDetailsActivity;
import com.aait.alphacareclient.UI.Adapters.ExperienceAdapter;
import com.aait.alphacareclient.UI.Adapters.GenderAdapter;
import com.aait.alphacareclient.UI.Adapters.ProviderAdapter;
import com.aait.alphacareclient.UI.Adapters.RatingAdapter;
import com.aait.alphacareclient.UI.Adapters.SavedAdapter;
import com.aait.alphacareclient.UI.Adapters.SectionAdapter;
import com.aait.alphacareclient.UI.Views.CustomInfoWindowOfMap;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.DialogUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import kotlin.jvm.Strictfp;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements OnItemClickListener, GoogleMap.OnMarkerClickListener, OnMapReadyCallback, GpsTrakerListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.map)
    MapView mapView;
    String mResult;
    GoogleMap googleMap;
    Marker myMarker , shopMarker;
    Geocoder geocoder;
    GPSTracker gps;
    boolean startTracker = false;
    public String mLang= "46.738586", mLat="24.774265";
    MarkerOptions markerOptions;
    private AlertDialog mAlertDialog;
    HashMap<Marker, HomeModel> hmap = new HashMap<>();
    ArrayList<HomeModel> shopModels = new ArrayList<>();

    String newToken= null;
    int flag = 0;
    @BindView(R.id.map_lay)
    RelativeLayout map_lay;
    @BindView(R.id.filter_lay)
    LinearLayout filter_lay;
    @BindView(R.id.sections)
    RecyclerView sections;
    @BindView(R.id.providers)
    RecyclerView providers;
    @BindView(R.id.gender)
    RecyclerView gender;
    @BindView(R.id.experience)
    RecyclerView experience;
    @BindView(R.id.saved_location)
            RecyclerView saved_location;
    @BindView(R.id.latest_location)
            TextView latest;
    @BindView(R.id.addresses_lay)
            LinearLayout address;
    @BindView(R.id.rates)
            RecyclerView rates;
    ArrayList<sectionModel> sectionModels = new ArrayList<>();
    ArrayList<sectionModel> provider = new ArrayList<>();
    ArrayList<sectionModel> genders = new ArrayList<>();
    ArrayList<sectionModel> experiences = new ArrayList<>();
    ArrayList<sectionModel> rating = new ArrayList<>();
    sectionModel section = new sectionModel(null,null);
    sectionModel Provider = new sectionModel(null,null) ;
    sectionModel Gender = new sectionModel(null,null);
    sectionModel Experience = new sectionModel(null,null) ;
    sectionModel Rate = new sectionModel(null,null) ;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager gridLayoutManager1;
    GridLayoutManager gridLayoutManager2;
    GridLayoutManager gridLayoutManager3;
    GridLayoutManager gridLayoutManager4;
    SectionAdapter sectionAdapter;
    ProviderAdapter sectionAdapter1;
    GenderAdapter genderAdapter;
    RatingAdapter ratingAdapter;
    ExperienceAdapter experienceAdapter;
    SavedAdapter savedAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<AddressesModel> addressesModels =new ArrayList<>();
    @BindView(R.id.reload)
            ImageView reload;
    int count = 0;
    int count1 = 0;
    @BindView(R.id.addresses)
    EditText addresses;

    public static HomeFragment newInstance(String token) {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        args.putString("token",token);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        title.setText(getString(R.string.home));
        reload.setVisibility(View.VISIBLE);

        menu.setVisibility(View.VISIBLE);
        provider.clear();
        genders.clear();
        experiences.clear();
        sectionModels.clear();
        rating.clear();
        sectionModels.add(0,new sectionModel("0",getString(R.string.all)));
        provider.add(new sectionModel("0",getString(R.string.all)));
        provider.add(new sectionModel("1",getString(R.string.Specialized_center)));
        provider.add(new sectionModel("2",getString(R.string.Technical)));
        rating.add(new sectionModel("-1",getString(R.string.all)));
        rating.add(new sectionModel("1",getString(R.string.highest_rate)));
        rating.add(new sectionModel("0",getString(R.string.lowest_rate)));
        genders.add(new sectionModel("0",getString(R.string.all)));
        genders.add(new sectionModel("1",getString(R.string.male)));
        genders.add(new sectionModel("2",getString(R.string.female)));
        experiences.add(new sectionModel("0",getString(R.string.all)));
        experiences.add(new sectionModel("1",getString(R.string.junior)));
        experiences.add(new sectionModel("2",getString(R.string.Average)));
        experiences.add(new sectionModel("3",getString(R.string.experienced)));
        experiences.add(new sectionModel("4",getString(R.string.senior)));


        Log.e("eeee",new Gson().toJson(provider)+"  ,"+new Gson().toJson(genders));
        gridLayoutManager = new GridLayoutManager(mContext,3);
        gridLayoutManager1 = new GridLayoutManager(mContext,2);
        gridLayoutManager2 = new GridLayoutManager(mContext,3);
        gridLayoutManager3 = new GridLayoutManager(mContext,3);
        gridLayoutManager4 = new GridLayoutManager(mContext,3);
        sectionAdapter = new SectionAdapter(mContext,sectionModels,R.layout.recycler_sections);
        sectionAdapter1 = new ProviderAdapter(mContext,provider,R.layout.recycler_provider);
        genderAdapter = new GenderAdapter(mContext,genders,R.layout.recycler_gender);
        ratingAdapter = new RatingAdapter(mContext,rating,R.layout.recycler_rate);
        experienceAdapter = new ExperienceAdapter(mContext,experiences,R.layout.recycler_experience);
        sectionAdapter.setOnItemClickListener(HomeFragment.this);
        sectionAdapter1.setOnItemClickListener(HomeFragment.this);
        genderAdapter.setOnItemClickListener(HomeFragment.this);
        experienceAdapter.setOnItemClickListener(HomeFragment.this);
        ratingAdapter.setOnItemClickListener(HomeFragment.this);
        rates.setLayoutManager(gridLayoutManager4);
        rates.setAdapter(ratingAdapter);
        sections.setLayoutManager(gridLayoutManager);
        sections.setAdapter(sectionAdapter);
        providers.setLayoutManager(gridLayoutManager1);
        providers.setAdapter(sectionAdapter1);
        gender.setLayoutManager(gridLayoutManager2);
        gender.setAdapter(genderAdapter);
        experience.setLayoutManager(gridLayoutManager3);
        experience.setAdapter(experienceAdapter);
        Log.e("ssss",new Gson().toJson(sectionModels));
//        Log.e("token",mSharedPrefManager.getUserData().getToken_id());
//        Log.e("device",newToken);
        getSections();
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        savedAdapter = new SavedAdapter(mContext,addressesModels,R.layout.recycler_saved);
        savedAdapter.setOnItemClickListener(HomeFragment.this);

        saved_location.setLayoutManager(linearLayoutManager);
        saved_location.setAdapter(savedAdapter);


        if (mSharedPrefManager.getLoginStatus()) {
            getAddress(newToken);
        }
                Log.e("newToken",newToken);

        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @SuppressLint("ResourceType")
    @OnClick(R.id.menu)
    void onMenu(){
        ListHomeFragment nextFrag= new ListHomeFragment().newInstance(mLat,mLang);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_fragment_container, nextFrag, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }

    @OnClick(R.id.filter)
    void onFilter(){
        if (address.getVisibility()==View.VISIBLE&&filter_lay.getVisibility()==View.VISIBLE){
            filter_lay.setVisibility(View.GONE);

        }else if (address.getVisibility()==View.VISIBLE&&filter_lay.getVisibility()==View.GONE){
            address.setVisibility(View.GONE);
            filter_lay.setVisibility(View.VISIBLE);
        }else if (address.getVisibility()==View.GONE&&filter_lay.getVisibility()==View.VISIBLE){
            filter_lay.setVisibility(View.GONE);
        }else if (address.getVisibility()==View.GONE&&filter_lay.getVisibility()==View.GONE){
            filter_lay.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                putMapMarker(lat, log);
            }
        }
    }
    @OnClick(R.id.search)
    void onSearch(){
        if (addresses.getText().toString().trim().isEmpty()){

        }else {
            if (mSharedPrefManager.getLoginStatus()) {
                getHome(mLat, mLang, newToken,mSharedPrefManager.getUserData().getToken_id(), addresses.getText().toString(), null, null, null, null, null);
            }else {
                getHome(mLat, mLang, null,null, addresses.getText().toString(), null, null, null, null, null);
            }
        }
    }
    @OnClick(R.id.reload)
    void onReload(){
        if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {

            if (mSharedPrefManager.getLoginStatus()) {
                getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
            } else {
                getHome(gps.getLatitude() + "", gps.getLongitude() + "", null, null, null, null, null, null, null, null);
            }
        }else {
            if (mSharedPrefManager.getLoginStatus()) {
                getHome(24.774265 + "", 46.738586 + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
            } else {
                getHome(24.774265 + "", 46.738586 + "", null, null, null, null, null, null, null, null);
            }
        }

    }
    private void getAddress(String token){

        RetroWeb.getClient().create(ServiceApi.class).getAddress(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getToken_id(),token,0).enqueue(new Callback<AddressesResponse>() {
            @Override
            public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {

                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){

                        }else {
                            savedAdapter.updateAll(response.body().getData());
                        }
                    }
                    else if (response.body().getKey().equals("2")){

                        Intent intent = new Intent(mContext, ActivateAccountActivity.class);
                        intent.putExtra("data",mSharedPrefManager.getUserData());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
    @Override
    public void onStartTracker() {
        startTracker = true;

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!marker.getTitle().equals("موقعي")){
            if (hmap.containsKey(marker)){
                final HomeModel myShopModel =hmap.get(marker);

                CustomInfoWindowOfMap customInfoWindowOfMap = new CustomInfoWindowOfMap(mContext, myShopModel);
                googleMap.setInfoWindowAdapter(customInfoWindowOfMap);
                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(mContext, ProviderDetailsActivity.class);
                        Log.e("id", String.valueOf(myShopModel.getProvider_id()));
                        intent.putExtra("id",myShopModel.getProvider_id()+"");
                        startActivity(intent);
                    }
                });

            }else {
                googleMap.setInfoWindowAdapter(null);
            }
        }else {
            googleMap.setInfoWindowAdapter(null);
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                }
            });
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        getLocationWithPermission();
    }

    public void putMapMarker(Double lat, Double log) {
        LatLng latLng = new LatLng(lat, log);
        myMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_green)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
// kkjgj

    }
    public void putMapMarker1(Double lat, Double log) {
        LatLng latLng = new LatLng(lat, log);
        myMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("موقعى")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_red)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
// kkjgj

    }
    @OnClick(R.id.down)
    void onAddress(){
       if (address.getVisibility()==View.VISIBLE&&filter_lay.getVisibility()==View.VISIBLE){
           address.setVisibility(View.GONE);

       }else if (address.getVisibility()==View.GONE&&filter_lay.getVisibility()==View.VISIBLE){
           address.setVisibility(View.VISIBLE);
           filter_lay.setVisibility(View.GONE);
       }else if (address.getVisibility()==View.VISIBLE&&filter_lay.getVisibility()==View.GONE){
           address.setVisibility(View.GONE);
       }else if(address.getVisibility()==View.GONE&&filter_lay.getVisibility()==View.GONE){
           address.setVisibility(View.VISIBLE);
       }
    }
    @OnClick(R.id.latest_location)
    void onLatest(){
        address.setVisibility(View.GONE);
        addresses.setText(mResult);
        putMapMarker1(Double.parseDouble(mLat),Double.parseDouble(mLang));
    }
    private void getHome(String lat, String lng, String token, String token_id, String name, String section, String type, String gender, String experience, String rate){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getHome(mLanguagePrefManager.getAppLanguage(),0,token_id,token,name,section,type,gender,experience,rate,lat,lng).enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Log.e("home",new Gson().toJson(response.body().getData()));
                        if (response.body().getData().size()!=0){
                            shopModels = response.body().getData();
                            Collections.reverse(shopModels);
                            Log.e("shop",new Gson().toJson(shopModels));

                            for ( int i = 0;i<shopModels.size();i++) {
                                addShop(shopModels.get(i));
                            }
                        }else {
                            googleMap.clear();
                            putMapMarker(gps.getLatitude(),gps.getLongitude());
                            CommonUtil.makeToast(mContext,getString(R.string.no_providers));
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                Log.e("fail",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();


            }
        });
    }

    void addShop(HomeModel shopModel ){

        markerOptions =
                new MarkerOptions().position(new LatLng(Float.parseFloat(shopModel.getAddress_lat()), Float.parseFloat(shopModel.getAddress_lng())))
                .title("Shop")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_blue));
        shopMarker = googleMap.addMarker(markerOptions);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(shopModel.getAddress_lat()), Double.parseDouble(shopModel.getAddress_lng())), 12f));
        hmap.put(shopMarker,shopModel);

    }

    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getActivity(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(getActivity(),
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()){
                        Toast.makeText(getActivity(), getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
                    else{mResult = addresses.get(0).getAddressLine(0);
                         latest.setText(mResult);
                    }

                } catch (IOException e) {}
                googleMap.clear();
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken,mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(mLat + "", mLang + "", newToken,mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(mLat + "", mLang + "", null,null, null, null, null, null, null, null);
                }
            }
        }
    }

    private void getSections(){
        RetroWeb.getClient().create(ServiceApi.class).getSections(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<SectionResponse>() {
            @Override
            public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()!=0){

//                            sectionModels.clear();
//                            sectionModels.add(0,new sectionModel("0",getString(R.string.all)));
                           // sectionModels.addAll(response.body().getData());
                            sectionModels = response.body().getData();
                            sectionModels.add(0,new sectionModel("0",getString(R.string.all)));
                            sectionAdapter.updateAll(sectionModels);
//                            Log.e("sections",new Gson().toJson(sectionModels));
//                            for (int i =0;i<sectionModels.size();i++){
//                                sectionAdapter.Insert(i,sectionModels.get(i));
//
//                            }
                            //sectionAdapter.InsertAll(sectionModels);
                            Log.e("data",new Gson().toJson(sectionAdapter.getData()));

//                            sectionAdapter.Insert(0,new sectionModel("0",getString(R.string.all)));
//                            sectionAdapter.InsertAll(response.body().getData());
//                            for (int i=0;i<sectionModels.size();i++){
//                                sectionAdapter.Insert(i,sectionModels.get(i));
//                            }
//                            sectionAdapter.updateAll(sectionModels);
                        }else {

                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SectionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

        if (view.getId()==R.id.section){
            filter_lay.setVisibility(View.GONE);

            section = sectionModels.get(position);
            if (sectionModels.get(position).getId().equals("0")) {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                }
                else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, null, null, null, null, null);
                }
               // getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId());
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken,mSharedPrefManager.getUserData().getToken_id(), null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }
            }
        }else if (view.getId() == R.id.provider){
            filter_lay.setVisibility(View.GONE);

            Provider = provider.get(position);
            if (provider.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null, null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), provider.get(position).getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, section.getId(), provider.get(position).getId(), Gender.getId(), Experience.getId(),Rate.getId());
                }
            }
        }else if (view.getId() == R.id.gender){
            filter_lay.setVisibility(View.GONE);

            Gender = genders.get(position);
            if (genders.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(gps.getLatitude()+"",gps.getLongitude()+"",null,null,null,null,null,null,null,null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), Provider.getId(), genders.get(position).getId(), Experience.getId(), Rate.getId());
                }
                else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, section.getId(), Provider.getId(), genders.get(position).getId(), Experience.getId(),Rate.getId());
                }
            }
        }else if (view.getId() == R.id.experience){
            filter_lay.setVisibility(View.GONE);

            Experience = experiences.get(position);
            experiences.get(position).setChecked(false);
            if (experiences.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null, null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {

                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(),Rate.getId());
                }
            }
        }else if(view.getId()==R.id.rate) {
            filter_lay.setVisibility(View.GONE);

            Rate = rating.get(position);
            rating.get(position).setChecked(false);
            if (rating.get(position).getId().equals("-1")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null, null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(gps.getLatitude() + "", gps.getLongitude() + "", null,null, null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(),Rate.getId());
                }
            }

        }else if (view.getId() == R.id.saved_locations){
            address.setVisibility(View.GONE);
            addresses.setText(addressesModels.get(position).getLatlng_address());
            putMapMarker1(Double.parseDouble(addressesModels.get(position).getLat()),Double.parseDouble(addressesModels.get(position).getLng()));
        }

    }
}
