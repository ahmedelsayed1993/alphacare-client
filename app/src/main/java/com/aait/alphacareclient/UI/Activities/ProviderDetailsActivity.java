package com.aait.alphacareclient.UI.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.CommentsModel;
import com.aait.alphacareclient.Models.ProviderDetailsModel;
import com.aait.alphacareclient.Models.ProviderDetailsResponse;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.Models.ToggleResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Adapters.CommentsAdapter;
import com.aait.alphacareclient.UI.Adapters.OffersAdapter;
import com.aait.alphacareclient.UI.Adapters.RecyclerPoupupAds;
import com.aait.alphacareclient.UI.Adapters.ServicesAdapter;
import com.aait.alphacareclient.UI.Views.RateDialog;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.aait.alphacareclient.Uitls.PermissionUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsActivity extends ParentActivity implements OnItemClickListener {

    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    String id;

    String newToken = null;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
   @BindView(R.id.name)
    TextView name;
   @BindView(R.id.rating)
    RatingBar rating;
   @BindView(R.id.title)
   TextView title;
   @BindView(R.id.description)
   TextView description;
   @BindView(R.id.open)
    ImageView open;
   @BindView(R.id.add_fav)
   ImageView addFav;
   private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerPoupupAds recyclerPoupupAds;
    ArrayList<String> banners = new ArrayList<>();
    ArrayList<ServicesModel> servicesModels = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    ServicesAdapter servicesAdapter;
    @BindView(R.id.services)
    RecyclerView services;
    @BindView(R.id.offers)
    RecyclerView offers;
    LinearLayoutManager linearLayoutManager;
    OffersAdapter offersAdapter;
    ArrayList<ServicesModel> offersModel = new ArrayList<>();
    @BindView(R.id.comments)
    RecyclerView comments;
    @BindView(R.id.phone)
            TextView phone;
    @BindView(R.id.no_services)
            TextView no_services;
    @BindView(R.id.no_offers)
            TextView no_offers;
    ArrayList<CommentsModel> commentsModels = new ArrayList<>();
    LinearLayoutManager linearLayoutManager1;
    CommentsAdapter commentsAdapter;
    ArrayList<ServicesModel> ids= new ArrayList<>();
    ArrayList<ServicesModel> offer = new ArrayList<>();
    int count = 0;
    @BindView(R.id.request)
    Button request;
    ProviderDetailsModel providerDetailsModel;
    @Override
    protected void initializeComponents() {
        if (ids.size()==0){
            request.setVisibility(View.GONE);
        }else {
            request.setVisibility(View.VISIBLE);
        }
        id = getIntent().getStringExtra("id");

        gridLayoutManager = new GridLayoutManager(mContext,2);
        servicesAdapter = new ServicesAdapter(mContext,servicesModels,R.layout.recycler_service);
        servicesAdapter.setOnItemClickListener(this);
        services.setLayoutManager(gridLayoutManager);
        services.setAdapter(servicesAdapter);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        offersAdapter = new OffersAdapter(mContext,offersModel,R.layout.recycler_offers);
        offersAdapter.setOnItemClickListener(this);
        offers.setLayoutManager(linearLayoutManager);
        offers.setAdapter(offersAdapter);
        linearLayoutManager1 = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        commentsAdapter = new CommentsAdapter(mContext,commentsModels,R.layout.recycler_comment);
        comments.setLayoutManager(linearLayoutManager1);
        comments.setAdapter(commentsAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ProviderDetailsActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                if (mSharedPrefManager.getLoginStatus()) {
                    getProvider(mSharedPrefManager.getUserData().getToken_id(),newToken);
                }else {
                    getProvider(null,null);
                }
                Log.e("newToken",newToken);

            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_provider_details;
    }
    void setData(ProviderDetailsModel providerDetailsModel){
        banners = providerDetailsModel.getProvider().getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(mContext,banners);
        viewPager.setAdapter(recyclerPoupupAds);

//                        indicator.setViewPager(viewPager);


        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){ currentPage = 0; }
                viewPager.setCurrentItem(currentPage,true);
                currentPage++;

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);


            }
        }, 2500,2500);

        phone.setText(getString(R.string.phone)+" : "+providerDetailsModel.getProvider().getPhone());
        name.setText(providerDetailsModel.getProvider().getName());
        rating.setRating(Float.parseFloat(providerDetailsModel.getProvider().getFinal_rate()));
        title.setText(providerDetailsModel.getProvider().getSection());
        description.setText(providerDetailsModel.getProvider().getDesc());
        if (providerDetailsModel.getProvider().isIs_fav()){
            addFav.setBackground(mContext.getResources().getDrawable(R.mipmap.fav_blue));
        }else {
            addFav.setBackground(mContext.getResources().getDrawable(R.mipmap.like));
        }
        if (providerDetailsModel.getProvider().getIs_online().equals("0")){
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.red));
        }else {
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.green));
        }
        servicesModels = providerDetailsModel.getServices();
        offersModel = providerDetailsModel.getOffers();
        if (servicesModels.size()==0){
            no_services.setVisibility(View.VISIBLE);
            services.setVisibility(View.GONE);
        }else {
            no_services.setVisibility(View.GONE);
            services.setVisibility(View.VISIBLE);
            servicesAdapter.updateAll(providerDetailsModel.getServices());
        }
        if (offersModel.size()==0){
            no_offers.setVisibility(View.VISIBLE);
            offers.setVisibility(View.GONE);
        }else {
            no_offers.setVisibility(View.GONE);
            offers.setVisibility(View.VISIBLE);
            offersAdapter.updateAll(providerDetailsModel.getOffers());
        }

        commentsAdapter.updateAll(providerDetailsModel.getComments());
    }

    private void getProvider(String token,String token_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(mLanguagePrefManager.getAppLanguage(),0,token,token_id,id).enqueue(new Callback<ProviderDetailsResponse>() {
            @Override
            public void onResponse(Call<ProviderDetailsResponse> call, Response<ProviderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Log.e("offers",new Gson().toJson(response.body().getData().getOffers()));
                        providerDetailsModel = response.body().getData();
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());

                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.add_fav)
    void onFav(){
        if (mSharedPrefManager.getLoginStatus()) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).toogleFav(mLanguagePrefManager.getAppLanguage(), 0, mSharedPrefManager.getUserData().getToken_id(), newToken, Integer.parseInt(id)).enqueue(new Callback<ToggleResponse>() {
                @Override
                public void onResponse(Call<ToggleResponse> call, Response<ToggleResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                            if (response.body().getData() == 1) {
                                addFav.setBackground(mContext.getResources().getDrawable(R.mipmap.fav_blue));
                            } else {
                                addFav.setBackground(mContext.getResources().getDrawable(R.mipmap.like));
                            }
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ToggleResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }
//        VCard vcard = new VCard();
//
//        StructuredName n = new StructuredName();
//        n.setFamily("Doe");
//        n.setGiven("Jonathan");
//        n.getPrefixes().add("Mr");
//        vcard.setStructuredName(n);
//
//        vcard.setFormattedName("John Doe");
//
//        String str = Ezvcard.write(vcard).version(VCardVersion.V4_0).go();
//        Log.e("ddd",str);
//        String json =
//                "[\"vcard\"," +
//                        "[" +
//                        "[\"version\", {}, \"text\", \"4.0\"]," +
//                        "[\"n\", {}, \"text\", [\"House\", \"Gregory\", \"\", \"Dr\", \"MD\"]]," +
//                        "[\"fn\", {}, \"text\", \"Dr. Gregory House M.D.\"]" +
//                        "]" +
//                        "]";
//
//        VCard vcard = Ezvcard.parseJson(json).first();

    }


    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.add){
//            count++;
//            if (count%2==1) {
                if (servicesModels.get(position).isSelected()){
                   // view.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                    view.setBackground(mContext.getResources().getDrawable(R.mipmap.add));
                    ids.remove(servicesModels.get(position));
                    servicesModels.get(position).setSelected(false);
                    //ids.remove(servicesModels.get(position).getId());
                    Log.e("ids", new Gson().toJson(ids));
                }else {
                   // view.setBackgroundColor(mContext.getResources().getColor(R.color.colorRed));
                    view.setBackground(mContext.getResources().getDrawable(R.mipmap.min));
                    ids.add(servicesModels.get(position));
                    servicesModels.get(position).setSelected(true);
                    Log.e("ids", new Gson().toJson(ids));
                }

//            }else {
//                if (servicesModels.get(position).isSelected()) {
//                    view.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
//                    ids.remove(Integer.valueOf(servicesModels.get(position).getId()));
//                    //ids.remove(servicesModels.get(position).getId());
//                    Log.e("ids", new Gson().toJson(ids));
//
//                }else {
//                    view.setBackgroundColor(mContext.getResources().getColor(R.color.colorRed));
//                    ids.add(servicesModels.get(position).getId());
//                    servicesModels.get(position).setSelected(true);
//                    Log.e("ids", new Gson().toJson(ids));
//                }
//            }
            if (offer.size()==0&&ids.size()==0){
                request.setVisibility(View.GONE);
            }else {
                request.setVisibility(View.VISIBLE);
                request.setText(getString(R.string.request_appointment)+"("+(ids.size()+offer.size())+getString(R.string.services)+")");

            }

        }
        else if (view.getId()==R.id.offer_lay){
            Intent intent = new Intent(mContext,ServiceDetailsActivity.class);
            intent.putExtra("id",offersModel.get(position).getId()+"");
            intent.putExtra("selected",offersModel.get(position).isSelected());
            intent.putExtra("position",position+"");
            intent.putExtra("offer","offer");
            startActivityForResult(intent,1);
//            offersModel.get(position).setSelected(true);
        }else if (view.getId() == R.id.service){
            Intent intent = new Intent(mContext,ServiceDetailsActivity.class);
            intent.putExtra("id",servicesModels.get(position).getId()+"");
            intent.putExtra("selected",servicesModels.get(position).isSelected());
            intent.putExtra("position",position+"");
            intent.putExtra("offer","service");
            startActivityForResult(intent,2);
//            servicesModels.get(position).setSelected(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                int i = Integer.parseInt(data.getStringExtra("position"));
//                Log.e("data",new Gson().toJson(data.getSerializableExtra("id")));
                offer.add((ServicesModel)data.getSerializableExtra("id"));
                offersModel.get(i).setSelected(true);
                offersAdapter.update(i,offersModel.get(i));

                if (offer.size()==0&&ids.size()==0){
                    request.setVisibility(View.GONE);
                }else {
                    request.setVisibility(View.VISIBLE);
                    request.setText(getString(R.string.request_appointment)+"("+(ids.size()+offer.size())+getString(R.string.services)+")");

                }
            }

        }else if (requestCode == 2){
            if(resultCode == Activity.RESULT_OK){
                int i = Integer.parseInt(data.getStringExtra("position"));
//                ServicesModel servicesModel = (ServicesModel)data.getSerializableExtra("id");
//                servicesModels.get(Integer.valueOf(servicesModel.getId())).setSelected(true);
                ids.add((ServicesModel)data.getSerializableExtra("id"));
                servicesModels.get(i).setSelected(true);
                servicesAdapter.update(i,servicesModels.get(i));

                if (offer.size()==0&&ids.size()==0){
                    request.setVisibility(View.GONE);
                }else {
                    request.setVisibility(View.VISIBLE);
                    request.setText(getString(R.string.request_appointment)+"("+(ids.size()+offer.size())+getString(R.string.services)+")");
                }
            }
        }
    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        mContext.startActivity(intent);
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions((Activity)mContext,PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    @OnClick(R.id.phone)
    void onCall(){
        getLocationWithPermission(providerDetailsModel.getProvider().getPhone());
    }

    @OnClick(R.id.add_comment)
    void onAddRate(){
        if (mSharedPrefManager.getLoginStatus()) {
            Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_add_rate);
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            EditText comment;
            RatingBar rating;
            Button done;
            comment = dialog.findViewById(R.id.comment);
            rating = dialog.findViewById(R.id.rating);
            done = dialog.findViewById(R.id.done);
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, comment, mContext.getString(R.string.your_comment))) {
                        return;
                    } else {
                        showProgressDialog(getString(R.string.please_wait));
                        RetroWeb.getClient().create(ServiceApi.class).rate(mLanguagePrefManager.getAppLanguage(), 0, mSharedPrefManager.getUserData().getToken_id(), newToken, (int) rating.getRating(), comment.getText().toString(), Integer.parseInt(id), null).enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().getKey().equals("1")) {
                                        CommonUtil.makeToast(mContext, response.body().getMsg());
                                        dialog.dismiss();
                                        getProvider(mSharedPrefManager.getUserData().getToken_id(),newToken);
                                    } else {
                                        CommonUtil.makeToast(mContext, response.body().getMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                CommonUtil.handleException(mContext, t);
                                t.printStackTrace();
                                hideProgressDialog();
                            }
                        });
                    }
                }
            });


            dialog.show();
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
            startActivity( new Intent(mContext,LoginActivity.class));
            finish();
        }
    }
    @OnClick(R.id.request)
    void onRequest(){
        if (mSharedPrefManager.getLoginStatus()) {
            Intent intent = new Intent(mContext, RequestAppointmentActivity.class);
            intent.putExtra("provider", providerDetailsModel.getProvider());
            intent.putExtra("services", ids);
            intent.putExtra("offers", offer);
            intent.putExtra("branches", providerDetailsModel.getBranches());
            startActivity(intent);
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
            startActivity( new Intent(mContext,LoginActivity.class));
            finish();
        }
    }
}
