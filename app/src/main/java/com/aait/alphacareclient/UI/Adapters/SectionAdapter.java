package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.HomeModel;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.R;

import java.util.List;

import butterknife.BindView;

public class SectionAdapter extends ParentRecyclerAdapter<sectionModel> {
    int selectedPosition = 0;
    public SectionAdapter(Context context, List<sectionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        SectionAdapter.ViewHolder holder = new SectionAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final SectionAdapter.ViewHolder viewHolder = (SectionAdapter.ViewHolder) holder;
        final sectionModel addressesModel = data.get(position);
        viewHolder.section.setText(addressesModel.getTitle());
        viewHolder.section.setChecked(selectedPosition == position);
        viewHolder.section.setTag(position);
        viewHolder.section.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemClickListener.onItemClick(view,position);
                selectedPosition =(Integer) viewHolder.section.getTag();
                notifyDataSetChanged();

            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.section)
        RadioButton section;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
