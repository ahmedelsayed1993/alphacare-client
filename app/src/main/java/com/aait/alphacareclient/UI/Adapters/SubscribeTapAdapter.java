package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.alphacareclient.R;

import com.aait.alphacareclient.UI.Fragments.FragmentCurrentOrders;
import com.aait.alphacareclient.UI.Fragments.FragmentFinished;
import com.aait.alphacareclient.UI.Fragments.FragmentNewOrders;


public class SubscribeTapAdapter extends FragmentPagerAdapter {

    private Context context;
    private String token;

    public SubscribeTapAdapter(Context context , FragmentManager fm,String token) {
        super(fm);
        this.context = context;
        this.token = token;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new FragmentNewOrders().newInstance(token);
        }else if (position == 1){
            return new FragmentCurrentOrders().newInstance(token);
        }else {
            return new FragmentFinished().newInstance(token);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.pending_approval);
        }else if (position == 1){
            return context.getString(R.string.underway);
        }else {
            return context.getString(R.string.finished);
        }
    }
}
