package com.aait.alphacareclient.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Models.NotificationCountRespons;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Fragments.FavouriteFragment;
import com.aait.alphacareclient.UI.Fragments.HomeFragment;
import com.aait.alphacareclient.UI.Fragments.MoreFragment;
import com.aait.alphacareclient.UI.Fragments.OrdersFragment;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Stack;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ParentActivity implements BottomNavigationView.OnNavigationItemSelectedListener,BottomNavigationView.OnNavigationItemReselectedListener {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    int selectedTab = 0;
    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    HomeFragment mHomeFragment;
    MoreFragment mMoreFragment;
    OrdersFragment mOrdersFragment;
    FavouriteFragment mFavouriteFragment;
    String newToken= null;
    int count = 0;
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( MainActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                mHomeFragment = HomeFragment.newInstance(newToken);
                mMoreFragment = MoreFragment.newInstance(newToken);
                mOrdersFragment= OrdersFragment.newInstance(newToken);
                mFavouriteFragment = FavouriteFragment.newInstance(newToken);
                fragmentManager = getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.home_fragment_container, mHomeFragment);
                transaction.add(R.id.home_fragment_container,mOrdersFragment);
                transaction.add(R.id.home_fragment_container,mFavouriteFragment);
                transaction.add(R.id.home_fragment_container,mMoreFragment);
                transaction.commit();
                bottomNavigation.setOnNavigationItemSelectedListener(MainActivity.this);
                bottomNavigation.setOnNavigationItemReselectedListener(MainActivity.this);
                bottomNavigation.setSelectedItemId(R.id.home);
                showHome(true);
                Log.e("newToken",newToken);

            }
        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.home:
                showHome(true);
                break;
            case R.id.list:
                showOrders(true);
                break;
            case R.id.fav:
                showFavourite(true);
                break;
            case R.id.more:
                showMenu(true);
                break;
        }
        return true;
    }
    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mMoreFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mHomeFragment);
        transaction.commit();
        selectedTab = R.id.home;



    }
    private void showMenu(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }

            transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
            transaction.replace(R.id.home_fragment_container, mMoreFragment);
            transaction.commit();
            selectedTab = R.id.more;




    }
    private void showOrders(final boolean b) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (b) {
                if (selectedTab != 0) {
                    tabsStack.push(selectedTab);
                }
            }
            transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mMoreFragment);
//        transaction.hide(mFavouriteFragment);
            transaction.replace(R.id.home_fragment_container, mOrdersFragment);
            transaction.commit();
            selectedTab = R.id.list;
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }



    }
    private void showFavourite(final boolean b) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (b) {
                if (selectedTab != 0) {
                    tabsStack.push(selectedTab);
                }
            }
            transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mMoreFragment);
//        transaction.hide(mOrdersFragment);
            transaction.replace(R.id.home_fragment_container, mFavouriteFragment);
            transaction.commit();
            selectedTab = R.id.fav;
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.login_first));
        }



    }
//    private void getCount(String token){
//        showProgressDialog(getString(R.string.please_wait));
//        RetroWeb.getClient().create(ServiceApi.class).getCount(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),token).enqueue(new Callback<NotificationCountRespons>() {
//            @Override
//            public void onResponse(Call<NotificationCountRespons> call, Response<NotificationCountRespons> response) {
//                hideProgressDialog();
//                if (response.isSuccessful()){
//                    if (response.body().getKey().equals("1")){
//                       count = response.body().getData();
//                    }else if (response.body().getKey().equals("2")&&response.body().getUser_status().equals("non-active")){
//
//                            Intent intent = new Intent(mContext,ActivateAccountActivity.class);
//                            intent.putExtra("data",mSharedPrefManager.getUserData());
//                            startActivity(intent);
//
//                    }else {
//                        CommonUtil.makeToast(mContext,response.body().getMsg());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<NotificationCountRespons> call, Throwable t) {
//                CommonUtil.handleException(mContext,t);
//                t.printStackTrace();
//                hideProgressDialog();
//
//            }
//        });
//    }
}
