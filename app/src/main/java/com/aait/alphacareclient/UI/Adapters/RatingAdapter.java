package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.R;

import java.util.List;

import butterknife.BindView;

public class RatingAdapter extends ParentRecyclerAdapter<sectionModel> {
    int selectedPosition = 0;
    public RatingAdapter(Context context, List<sectionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        RatingAdapter.ViewHolder holder = new RatingAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final RatingAdapter.ViewHolder viewHolder = (RatingAdapter.ViewHolder) holder;
        final sectionModel addressesModel = data.get(position);
        viewHolder.section.setText(addressesModel.getTitle());
        viewHolder.section.setChecked(selectedPosition == position);
        viewHolder.section.setTag(position);
        viewHolder.section.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemClickListener.onItemClick(view,position);
                selectedPosition =(Integer) viewHolder.section.getTag();
                notifyDataSetChanged();

            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.rate)
        RadioButton section;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

