package com.aait.alphacareclient.UI.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareclient.Base.BaseFragment;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.HomeModel;
import com.aait.alphacareclient.Models.HomeResponse;
import com.aait.alphacareclient.Models.ProviderListResponse;
import com.aait.alphacareclient.Models.SectionResponse;
import com.aait.alphacareclient.Models.ToggleResponse;
import com.aait.alphacareclient.Models.providerListModel;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Activities.AddressesActivity;
import com.aait.alphacareclient.UI.Activities.LoginActivity;
import com.aait.alphacareclient.UI.Activities.ProviderDetailsActivity;
import com.aait.alphacareclient.UI.Adapters.ExperienceAdapter;
import com.aait.alphacareclient.UI.Adapters.GenderAdapter;
import com.aait.alphacareclient.UI.Adapters.HomeAdapter;
import com.aait.alphacareclient.UI.Adapters.ProviderAdapter;
import com.aait.alphacareclient.UI.Adapters.RatingAdapter;
import com.aait.alphacareclient.UI.Adapters.SavedAdapter;
import com.aait.alphacareclient.UI.Adapters.SectionAdapter;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListHomeFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<providerListModel> homeModels = new ArrayList<>();
    HomeAdapter homeAdapter;
    String lat;
    String lng;
    String newToken= null;
    @BindView(R.id.text)
    EditText text;
    @BindView(R.id.filter)
    LinearLayout filter_lay;
    @BindView(R.id.sections)
    RecyclerView sections;
    @BindView(R.id.providers)
    RecyclerView providers;
    @BindView(R.id.gender)
    RecyclerView gender;
    @BindView(R.id.experience)
    RecyclerView experience;
    @BindView(R.id.rates)
    RecyclerView rates;
    ArrayList<sectionModel> sectionModels = new ArrayList<>();
    ArrayList<sectionModel> provider = new ArrayList<>();
    ArrayList<sectionModel> genders = new ArrayList<>();
    ArrayList<sectionModel> experiences = new ArrayList<>();
    ArrayList<sectionModel> rating = new ArrayList<>();
    sectionModel section = new sectionModel(null,null);
    sectionModel Provider = new sectionModel(null,null) ;
    sectionModel Gender = new sectionModel(null,null);
    sectionModel Experience = new sectionModel(null,null) ;
    sectionModel Rate = new sectionModel(null,null) ;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager gridLayoutManager1;
    GridLayoutManager gridLayoutManager2;
    GridLayoutManager gridLayoutManager3;
    GridLayoutManager gridLayoutManager4;
    SectionAdapter sectionAdapter;
    ProviderAdapter sectionAdapter1;
    GenderAdapter genderAdapter;
    ExperienceAdapter experienceAdapter;
    RatingAdapter ratingAdapter;
    @BindView(R.id.reload)
            ImageView reload;
    int count = 0;
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home_list;
    }
    public static ListHomeFragment newInstance(String lat,String lng) {
        Bundle args = new Bundle();
        ListHomeFragment fragment = new ListHomeFragment();
        args.putString("lat",lat);
        args.putString("lng",lng);
        fragment.setArguments(args);
        return fragment;
    }
    @SuppressLint("ResourceType")
    @OnClick(R.id.menu)
    void onMenu(){
        HomeFragment nextFrag= new HomeFragment().newInstance(newToken);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_fragment_container, nextFrag, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }
    @Override
    protected void initializeComponents(View view) {
        title.setText(getString(R.string.home));
        reload.setVisibility(View.GONE);
        menu.setVisibility(View.VISIBLE);
        menu.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.map));
        Bundle bundle = this.getArguments();
        lat = bundle.getString("lat");
        lng = bundle.getString("lng");
        rating.clear();
        provider.add(new sectionModel("0",getString(R.string.all)));
        provider.add(new sectionModel("1",getString(R.string.Specialized_center)));
        provider.add(new sectionModel("2",getString(R.string.Technical)));
        rating.add(new sectionModel("-1",getString(R.string.all)));
        rating.add(new sectionModel("1",getString(R.string.highest_rate)));
        rating.add(new sectionModel("0",getString(R.string.lowest_rate)));
        genders.add(new sectionModel("0",getString(R.string.all)));
        genders.add(new sectionModel("1",getString(R.string.male)));
        genders.add(new sectionModel("2",getString(R.string.female)));
        experiences.add(new sectionModel("0",getString(R.string.all)));
        experiences.add(new sectionModel("1",getString(R.string.junior)));
        experiences.add(new sectionModel("2",getString(R.string.Average)));
        experiences.add(new sectionModel("3",getString(R.string.experienced)));
        experiences.add(new sectionModel("4",getString(R.string.senior)));
        Log.e("eeee",new Gson().toJson(provider)+"  ,"+new Gson().toJson(genders));
        gridLayoutManager = new GridLayoutManager(mContext,3);
        gridLayoutManager1 = new GridLayoutManager(mContext,2);
        gridLayoutManager2 = new GridLayoutManager(mContext,3);
        gridLayoutManager3 = new GridLayoutManager(mContext,3);
        gridLayoutManager4 = new GridLayoutManager(mContext,3);
        sectionAdapter = new SectionAdapter(mContext,sectionModels,R.layout.recycler_sections);
        sectionAdapter1 = new ProviderAdapter(mContext,provider,R.layout.recycler_provider);
        genderAdapter = new GenderAdapter(mContext,genders,R.layout.recycler_gender);
        ratingAdapter = new RatingAdapter(mContext,rating,R.layout.recycler_rate);
        experienceAdapter = new ExperienceAdapter(mContext,experiences,R.layout.recycler_experience);

        sectionAdapter.setOnItemClickListener(ListHomeFragment.this);
        sectionAdapter1.setOnItemClickListener(ListHomeFragment.this);
        genderAdapter.setOnItemClickListener(ListHomeFragment.this);
        experienceAdapter.setOnItemClickListener(ListHomeFragment.this);
        ratingAdapter.setOnItemClickListener(ListHomeFragment.this);
        rates.setLayoutManager(gridLayoutManager4);
        rates.setAdapter(ratingAdapter);
        sections.setLayoutManager(gridLayoutManager);
        sections.setAdapter(sectionAdapter);
        providers.setLayoutManager(gridLayoutManager1);
        providers.setAdapter(sectionAdapter1);
        gender.setLayoutManager(gridLayoutManager2);
        gender.setAdapter(genderAdapter);
        experience.setLayoutManager(gridLayoutManager3);
        experience.setAdapter(experienceAdapter);
        getSections();

        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        homeAdapter = new HomeAdapter(mContext,homeModels,R.layout.recycler_providers);
        homeAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(homeAdapter);

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        if (mSharedPrefManager.getLoginStatus()) {
                            getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                        }else {
                            getHome(null,null,null,null,null,null,null,null);
                        }
                        Log.e("newToken",newToken);

                    }
                });
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                }else {
                    getHome(null,null,null,null,null,null,null,null);
                }
                Log.e("newToken",newToken);

            }
        });


    }
    @OnClick(R.id.search)
    void onSearch(){
        if (text.getText().toString().trim().isEmpty()){

        }else {
            if (mSharedPrefManager.getLoginStatus()) {
                getHome(newToken, mSharedPrefManager.getUserData().getToken_id(), text.getText().toString(), null, null, null, null, null);
            }else {
                getHome(null, null, text.getText().toString(), null, null, null, null, null);
            }
        }
    }
    private void getSections(){
        RetroWeb.getClient().create(ServiceApi.class).getSections(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<SectionResponse>() {
            @Override
            public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()!=0){

                            sectionModels = response.body().getData();
                            sectionModels.add(0,new sectionModel("0",getString(R.string.all)));
                            sectionAdapter.updateAll(sectionModels);
                        }else {

                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SectionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
    @OnClick(R.id.filter1)
    void onFilter(){
        count++;
        if (count%2==1) {

            filter_lay.setVisibility(View.VISIBLE);
        }else {
//            provider.clear();
//            genders.clear();
//            experiences.clear();
            Log.e("nnnn",new Gson().toJson(provider)+"  ,"+new Gson().toJson(genders));
            filter_lay.setVisibility(View.GONE);
        }
    }
    private void getHome(String token,String token_id,String name,String section,String type,String gender,String experience,String rate){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getList(mLanguagePrefManager.getAppLanguage(),0,token_id,token,name,section,type,gender,experience,rate).enqueue(new Callback<ProviderListResponse>() {
            @Override
            public void onResponse(Call<ProviderListResponse> call, Response<ProviderListResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Log.e("home",new Gson().toJson(response.body().getData()));
                        if (response.body().getData().size()!=0){
                           homeAdapter.updateAll(response.body().getData());
                        }else {
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderListResponse> call, Throwable t) {
                Log.e("fail",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);


            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.fav){
            if (mSharedPrefManager.getLoginStatus()) {
                AddFav(homeModels.get(position).getId());
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.login_first));
                startActivity( new Intent(mContext, LoginActivity.class));
                getActivity().finish();
            }
        }

       else if (view.getId()==R.id.section){
            filter_lay.setVisibility(View.GONE);

            section = sectionModels.get(position);
            if (sectionModels.get(position).getId().equals("0")) {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                }else {
                    getHome(null, null,null, null, null, null, null, null);
                }
                // getHome(gps.getLatitude() + "", gps.getLongitude() + "", newToken, null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId());
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(null, null,null, sectionModels.get(position).getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }
            }
        }else if (view.getId() == R.id.provider){
            filter_lay.setVisibility(View.GONE);

            Provider = provider.get(position);
            if (provider.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                }else {
                    getHome(null, null,null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, section.getId(), provider.get(position).getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(null, null,null, section.getId(), provider.get(position).getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }
            }
        }else if (view.getId() == R.id.gender){
            filter_lay.setVisibility(View.GONE);

            Gender = genders.get(position);
            if (genders.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken,mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(null,null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, section.getId(), Provider.getId(), genders.get(position).getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(null, null,null, section.getId(), Provider.getId(), genders.get(position).getId(), Experience.getId(), Rate.getId());
                }
            }
        }else if (view.getId() == R.id.experience){
            filter_lay.setVisibility(View.GONE);

            Experience = experiences.get(position);
            experiences.get(position).setChecked(false);
            if (experiences.get(position).getId().equals("0")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken, mSharedPrefManager.getUserData().getToken_id(),null, null, null, null, null, null);
                }else {
                    getHome(null, null,null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken,mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(null,null, null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }
            }
        }else if(view.getId()==R.id.rate) {
            filter_lay.setVisibility(View.GONE);

            Rate = rating.get(position);
            rating.get(position).setChecked(false);
            if (rating.get(position).getId().equals("-1")){
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken,mSharedPrefManager.getUserData().getToken_id(), null, null, null, null, null, null);
                }else {
                    getHome(null,null, null, null, null, null, null, null);
                }
            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    getHome(newToken,mSharedPrefManager.getUserData().getToken_id(), null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }else {
                    getHome(null,null, null, section.getId(), Provider.getId(), Gender.getId(), Experience.getId(), Rate.getId());
                }
            }

        }else {
            Intent intent = new Intent(mContext, ProviderDetailsActivity.class);
            Log.e("id", String.valueOf(homeModels.get(position).getId()));
            intent.putExtra("id",homeModels.get(position).getId()+"");
            startActivity(intent);
        }

    }
    private void AddFav(int provider_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).toogleFav(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id(),newToken,provider_id).enqueue(new Callback<ToggleResponse>() {
            @Override
            public void onResponse(Call<ToggleResponse> call, Response<ToggleResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        getHome(newToken,mSharedPrefManager.getUserData().getToken_id(),null,null,null,null,null,null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ToggleResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
