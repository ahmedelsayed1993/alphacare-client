package com.aait.alphacareclient.UI.Views;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.alphacareclient.Models.HomeModel;

import com.aait.alphacareclient.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class CustomInfoWindowOfMap implements GoogleMap.InfoWindowAdapter {

    private Context mContext;
    private HomeModel shopModel;

    public CustomInfoWindowOfMap(Context mContext , HomeModel shopModel){
        this.mContext = mContext;
        this.shopModel = shopModel;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View viewMarker = ((Activity) mContext).getLayoutInflater().inflate(R.layout.custom_info,null);
//        FrameLayout custom_marker_view = viewMarker.findViewById(R.id.custom_marker_view);

        ImageView image = viewMarker.findViewById(R.id.image);
        TextView distance = viewMarker.findViewById(R.id.distance);
        TextView shop_name = viewMarker.findViewById(R.id.name);
        TextView address = viewMarker.findViewById(R.id.address);
        RatingBar rating = viewMarker.findViewById(R.id.rating);
        ImageView open = viewMarker.findViewById(R.id.open);

        String drawableRes=shopModel.getAvatar();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url = new URL(drawableRes);
            image.setImageBitmap(BitmapFactory.decodeStream((InputStream)url.getContent()));
        } catch (IOException e) {
            Log.e("error", e.getMessage());
        }

        if (shopModel.getProvider_isOnline().equals("0")){
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.red));
        }else {
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.green));
        }

        address.setText(shopModel.getSection_title());
        distance.setText(shopModel.getDistance());
        shop_name.setText(shopModel.getProvider_name());
        rating.setRating(Float.parseFloat(shopModel.getFinal_rate()));

        return viewMarker;

    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
