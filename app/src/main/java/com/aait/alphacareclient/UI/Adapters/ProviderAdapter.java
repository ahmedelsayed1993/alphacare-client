package com.aait.alphacareclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import com.aait.alphacareclient.Base.ParentRecyclerAdapter;
import com.aait.alphacareclient.Base.ParentRecyclerViewHolder;
import com.aait.alphacareclient.Models.sectionModel;
import com.aait.alphacareclient.R;

import java.util.List;

import butterknife.BindView;

public class ProviderAdapter extends ParentRecyclerAdapter<sectionModel> {
    int selectedPosition = 0;
    public ProviderAdapter(Context context, List<sectionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ProviderAdapter.ViewHolder holder = new ProviderAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ProviderAdapter.ViewHolder viewHolder = (ProviderAdapter.ViewHolder) holder;
        final sectionModel addressesModel = data.get(position);
        viewHolder.provider.setText(addressesModel.getTitle());
        viewHolder.provider.setChecked(selectedPosition == position);
        viewHolder.provider.setTag(position);
        viewHolder.provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position);
                selectedPosition =(Integer) viewHolder.provider.getTag();
                notifyDataSetChanged();
            }
        });
    }

    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.provider)
        RadioButton provider;


        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

