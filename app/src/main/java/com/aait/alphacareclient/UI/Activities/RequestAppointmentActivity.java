package com.aait.alphacareclient.UI.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareclient.App.Constant;
import com.aait.alphacareclient.Base.ParentActivity;
import com.aait.alphacareclient.Listeners.OnItemClickListener;
import com.aait.alphacareclient.Models.AddressesModel;
import com.aait.alphacareclient.Models.AddressesResponse;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.ProviderModel;
import com.aait.alphacareclient.Models.ServicesModel;
import com.aait.alphacareclient.Models.UserResponse;
import com.aait.alphacareclient.Network.RetroWeb;
import com.aait.alphacareclient.Network.ServiceApi;
import com.aait.alphacareclient.R;
import com.aait.alphacareclient.UI.Adapters.OffersAdapter;
import com.aait.alphacareclient.UI.Adapters.SavedAdapter;
import com.aait.alphacareclient.UI.Adapters.ServicesAdapter;
import com.aait.alphacareclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Pix;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestAppointmentActivity extends ParentActivity implements OnItemClickListener {
    ProviderModel providerModel;
    ArrayList<ServicesModel> servicesModels = new ArrayList<>();
    ArrayList<ServicesModel> offers = new ArrayList<>();
    @BindView(R.id.service)
    RecyclerView service;
    @BindView(R.id.offer)
    RecyclerView offer;
    LinearLayoutManager linearLayoutManager;
    OffersAdapter offersAdapter;
    GridLayoutManager gridLayoutManager;
    ServicesAdapter servicesAdapter;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.rating)
    RatingBar rating;
    int count=0,count1=0,count2 = 0;
    SavedAdapter savedAdapter;
    LinearLayoutManager linearLayoutManager1;
    ArrayList<AddressesModel> addressesModels =new ArrayList<>();
    @BindView(R.id.branches)
    TextView branches;
    @BindView(R.id.branch_list)
    RecyclerView branch_list;
    String newToken=null;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.day)
    TextView day;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.difficulties)
    EditText difficulties;
    String mAdresse="", mLang=null, mLat = null, mAddress="",Address = "";
    int blood = 1;
    @BindView(R.id.yes)
    RadioButton yes;
    @BindView(R.id.no)
            RadioButton no;
    @BindView(R.id.tell)
            TextView tell;
    @BindView(R.id.question)
            TextView question;
    ArrayList<Integer> ids = new ArrayList<>();
    AddressesModel addressesModel;
    String Id = "";
    String ID = "";
    String IDS = "";
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.request_appointment));
        if (blood==1){
           yes.setChecked(true);
           difficulties.setVisibility(View.VISIBLE);
           tell.setVisibility(View.VISIBLE);
        }else {
            no.setChecked(true);
            difficulties.setVisibility(View.GONE);
            tell.setVisibility(View.GONE);
        }
        providerModel = (ProviderModel) getIntent().getSerializableExtra("provider");
        servicesModels = (ArrayList<ServicesModel>)getIntent().getSerializableExtra("services");
        offers = (ArrayList<ServicesModel>) getIntent().getSerializableExtra("offers");
        addressesModels = (ArrayList<AddressesModel>) getIntent().getSerializableExtra("branches");
        Log.e("provider",new Gson().toJson(providerModel));
        Log.e("services",new Gson().toJson(servicesModels));
        Log.e("offers",new Gson().toJson(offers));
        gridLayoutManager = new GridLayoutManager(mContext,2);
        servicesAdapter = new ServicesAdapter(mContext,servicesModels,R.layout.recycler_service);
        servicesAdapter.setOnItemClickListener(this);
        service.setLayoutManager(gridLayoutManager);
        service.setAdapter(servicesAdapter);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        offersAdapter = new OffersAdapter(mContext,offers,R.layout.recycler_offers);
        offersAdapter.setOnItemClickListener(this);
        offer.setLayoutManager(linearLayoutManager);
        offer.setAdapter(offersAdapter);
        Glide.with(mContext).load(providerModel.getImages().get(0)).apply(new RequestOptions().error(R.mipmap.logo)).into(image);
        name.setText(providerModel.getName());
        address.setText(providerModel.getSection());
        rating.setRating(Float.parseFloat(providerModel.getFinal_rate()));
        if (providerModel.getSection_id().equals("1")){
            question.setText(getString(R.string.blood_process));

        }else if (providerModel.getSection_id().equals("2")){
            question.setText(getString(R.string.Do_you_face_any_difficulty_in_cupping));

        }else if (providerModel.getSection_id().equals("3")){
            question.setText(getString(R.string.Do_you_face_any_difficulty_in_physical_therapy_sessions));

        }else if (providerModel.getSection_id().equals("4")){
            question.setText(getString(R.string.Do_you_face_any_difficulty_in_changing_the_bandages));

        }
        linearLayoutManager1 = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        savedAdapter = new SavedAdapter(mContext,addressesModels,R.layout.recycler_saved);
        savedAdapter.setOnItemClickListener(this);
        branch_list.setLayoutManager(linearLayoutManager1);
        branch_list.setAdapter(savedAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( RequestAppointmentActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();

                Log.e("newToken",newToken);

            }
        });
        for (int i=0;i<servicesModels.size();i++){
            ids.add(servicesModels.get(i).getId());
        }
        Log.e("id",new Gson().toJson(ids));
        for (int i=0;i<offers.size();i++){
            ids.add(offers.get(i).getId());
        }
        Log.e("idd",new Gson().toJson(ids));
        Id = new Gson().toJson(ids);
       ID = Id.replace("[","");
       IDS = ID.replace("]","");
        Log.e("ID",IDS);

    }
    @OnClick(R.id.yes)
    void onYes(){
        blood = 1;
        difficulties.setVisibility(View.VISIBLE);
        tell.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.no)
    void onNo(){
        blood = 0;
        difficulties.setVisibility(View.GONE);
        tell.setVisibility(View.GONE);
    }
    @OnClick(R.id.services)
    void onServices(){
        count++;
        if (count%2==1) {
            service.setVisibility(View.VISIBLE);
        }else {
            service.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.offers)
    void onOffers(){
        count1++;
        if (count1%2==1) {
            offer.setVisibility(View.VISIBLE);
        }else {
            offer.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.branches)
    void onBranches(){
        count2++;
        if(count2%2==1){
            branch_list.setVisibility(View.VISIBLE);
        }else {
            branch_list.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.day)
    void onDay(){
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                day.setText(dayOfMonth + "-" + (monthOfYear+1) + "-" + year);
            }};

        DatePickerDialog dialog =
                new DatePickerDialog(this, listener, mYear, mMonth, mDay);
        dialog.show();
    }
    @OnClick(R.id.time)
    void onTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText( selectedHour + "." + selectedMinute);
            }
        }, hour, minute, false);
        //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    @OnClick(R.id.location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_request_appointment;
    }


    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.add){
            servicesModels.remove(servicesModels.get(position));
            ids.clear();
            servicesAdapter.notifyDataSetChanged();

            for (int i=0;i<servicesModels.size();i++){
                ids.add(servicesModels.get(i).getId());
            }
            Log.e("id",new Gson().toJson(ids));
            for (int i=0;i<offers.size();i++){
                ids.add(offers.get(i).getId());
            }
            Log.e("idd",new Gson().toJson(ids));
            Id = new Gson().toJson(ids);
            ID = Id.replace("[","");
            IDS = ID.replace("]","");
            Log.e("ID",IDS);
        }else if (view.getId()==R.id.checked){
            Log.e("of",offers.get(position).getId()+"");
            offers.remove(offers.get(position));
            ids.clear();
            offersAdapter.notifyDataSetChanged();
            for (int i=0;i<servicesModels.size();i++){
                ids.add(servicesModels.get(i).getId());
            }
            Log.e("id",new Gson().toJson(ids));
            for (int i=0;i<offers.size();i++){
                ids.add(offers.get(i).getId());
            }
            Log.e("idd",new Gson().toJson(ids));
            Id = new Gson().toJson(ids);
            ID = Id.replace("[","");
            IDS = ID.replace("]","");
            Log.e("ID",IDS);
        }else if (view.getId()== R.id.saved_locations){
            branch_list.setVisibility(View.GONE);
            addressesModel = addressesModels.get(position);
            branches.setText(addressesModel.getLatlng_address());
        }

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    Address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        location.setText(mAddress);
                    } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                        location.setText(mAdresse);
                    }

                }
            }
        }
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError(branches,getString(R.string.choose_branch))||
        CommonUtil.checkTextError(location,getString(R.string.location))||
        CommonUtil.checkTextError(day,getString(R.string.day))||
        CommonUtil.checkTextError(time,getString(R.string.time))){
            return;
        }else {
            if (IDS.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.services));
            }else {
                if (blood==2){
                    CommonUtil.makeToast(mContext,getString(R.string.blood_process));
                }else {
                    if (blood==2){
                        CommonUtil.makeToast(mContext,getString(R.string.blood_process));
                    }else {
                        if (blood==0) {
                            AddOrder(null);
                        }else if (blood == 1){
                            if (CommonUtil.checkTextError((AppCompatActivity)mContext,difficulties,getString(R.string.tell_us))){
                                return;
                            }else {
                                AddOrder(difficulties.getText().toString());
                            }
                        }
                    }
                }
            }
        }
    }

    private void AddOrder(String difficalt){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).AddOrder(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id()
        ,newToken,providerModel.getId(),addressesModel.getId(),IDS,mLat,mLang,mAddress,mAdresse,day.getText().toString(),time.getText().toString(),blood,difficalt).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Intent intent = new Intent(mContext,DoneActivity.class);
                        intent.putExtra("msg",response.body().getMsg());
                        startActivity(intent);
                        RequestAppointmentActivity.this.finish();
                    }else if (response.body().getKey().equals("2")&&response.body().getUser_status().equals("non-active")){

                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("data",mSharedPrefManager.getUserData());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
