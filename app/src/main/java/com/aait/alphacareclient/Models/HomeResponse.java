package com.aait.alphacareclient.Models;

import java.util.ArrayList;

public class HomeResponse extends BaseResponse{
    private ArrayList<HomeModel> data;

    public ArrayList<HomeModel> getData() {
        return data;
    }

    public void setData(ArrayList<HomeModel> data) {
        this.data = data;
    }
}
