package com.aait.alphacareclient.Models;

public class OrderDetailsResponse extends BaseResponse {
    private OrderDetailsModdel data;

    public OrderDetailsModdel getData() {
        return data;
    }

    public void setData(OrderDetailsModdel data) {
        this.data = data;
    }
}
