package com.aait.alphacareclient.Models;

import java.util.ArrayList;

public class ListResonse extends BaseResponse {
    private ArrayList<ListModel> data;

    public ArrayList<ListModel> getData() {
        return data;
    }

    public void setData(ArrayList<ListModel> data) {
        this.data = data;
    }
}
