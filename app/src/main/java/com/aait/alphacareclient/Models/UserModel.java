package com.aait.alphacareclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class UserModel implements Serializable {
    private int id;
    private String name;
    private String phone;
    private String email;
    private String gender;
    private String gender_name;
    private String national_id;
    private String national_copy;
    private String user_type;
    private String is_active;
    private String is_online;
    private String mobile_activation;
    private String code;
    private String lang;
    private String count_rate;
    private String final_rate;
    private String notification_status;
    private String avatar;
    private String token_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender_name() {
        return gender_name;
    }

    public void setGender_name(String gender_name) {
        this.gender_name = gender_name;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public String getNational_copy() {
        return national_copy;
    }

    public void setNational_copy(String national_copy) {
        this.national_copy = national_copy;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIs_online() {
        return is_online;
    }

    public void setIs_online(String is_online) {
        this.is_online = is_online;
    }

    public String getMobile_activation() {
        return mobile_activation;
    }

    public void setMobile_activation(String mobile_activation) {
        this.mobile_activation = mobile_activation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCount_rate() {
        return count_rate;
    }

    public void setCount_rate(String count_rate) {
        this.count_rate = count_rate;
    }

    public String getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(String final_rate) {
        this.final_rate = final_rate;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public static class FavResponse extends BaseResponse {
        private ArrayList<UserResponse.FavModel> data;

        public ArrayList<UserResponse.FavModel> getData() {
            return data;
        }

        public void setData(ArrayList<UserResponse.FavModel> data) {
            this.data = data;
        }
    }
}
