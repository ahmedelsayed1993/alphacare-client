package com.aait.alphacareclient.Models;

import java.util.ArrayList;

public class SectionResponse extends BaseResponse{
    private ArrayList<sectionModel> data;

    public ArrayList<sectionModel> getData() {
        return data;
    }

    public void setData(ArrayList<sectionModel> data) {
        this.data = data;
    }
}
