package com.aait.alphacareclient.Models;

import java.io.Serializable;

public class HomeModel implements Serializable {
    private int address_id;
    private String address_lat;
    private String address_lng;
    private String provider_name;
    private int provider_id;
    private String avatar;
    private int count_rate;
    private String final_rate;
    private int section_id;
    private String distance;
    private String section_title;
    private String provider_isOnline;
    private boolean isFav;

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }

    public String getProvider_isOnline() {
        return provider_isOnline;
    }

    public void setProvider_isOnline(String provider_isOnline) {
        this.provider_isOnline = provider_isOnline;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getAddress_lat() {
        return address_lat;
    }

    public void setAddress_lat(String address_lat) {
        this.address_lat = address_lat;
    }

    public String getAddress_lng() {
        return address_lng;
    }

    public void setAddress_lng(String address_lng) {
        this.address_lng = address_lng;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCount_rate() {
        return count_rate;
    }

    public void setCount_rate(int count_rate) {
        this.count_rate = count_rate;
    }

    public String getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(String final_rate) {
        this.final_rate = final_rate;
    }

    public int getSection_id() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSection_title() {
        return section_title;
    }

    public void setSection_title(String section_title) {
        this.section_title = section_title;
    }
}
