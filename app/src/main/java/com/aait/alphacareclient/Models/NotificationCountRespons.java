package com.aait.alphacareclient.Models;

public class NotificationCountRespons extends BaseResponse{
    private int data;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
