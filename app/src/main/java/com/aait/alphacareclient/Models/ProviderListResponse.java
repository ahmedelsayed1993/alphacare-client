package com.aait.alphacareclient.Models;

import java.util.ArrayList;

public class ProviderListResponse extends BaseResponse{
   private ArrayList<providerListModel> data;

    public ArrayList<providerListModel> getData() {
        return data;
    }

    public void setData(ArrayList<providerListModel> data) {
        this.data = data;
    }
}
