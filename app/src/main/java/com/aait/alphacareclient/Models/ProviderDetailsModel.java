package com.aait.alphacareclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProviderDetailsModel implements Serializable {
    private ProviderModel provider;
    private ArrayList<ServicesModel> services;
    private ArrayList<ServicesModel> offers;
    private ArrayList<CommentsModel> comments;
    private ArrayList<AddressesModel> branches;

    public ArrayList<AddressesModel> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<AddressesModel> branches) {
        this.branches = branches;
    }

    public ProviderModel getProvider() {
        return provider;
    }

    public void setProvider(ProviderModel provider) {
        this.provider = provider;
    }

    public ArrayList<ServicesModel> getServices() {
        return services;
    }

    public void setServices(ArrayList<ServicesModel> services) {
        this.services = services;
    }

    public ArrayList<ServicesModel> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<ServicesModel> offers) {
        this.offers = offers;
    }

    public ArrayList<CommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsModel> comments) {
        this.comments = comments;
    }
}
