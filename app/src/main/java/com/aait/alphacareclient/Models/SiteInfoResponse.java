package com.aait.alphacareclient.Models;

public class SiteInfoResponse extends BaseResponse {
    private SiteInfoModel data;

    public SiteInfoModel getData() {
        return data;
    }

    public void setData(SiteInfoModel data) {
        this.data = data;
    }
}
