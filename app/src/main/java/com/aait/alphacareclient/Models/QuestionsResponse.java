package com.aait.alphacareclient.Models;



public class QuestionsResponse extends BaseResponse {
    private QuestionsModel data;

    public QuestionsModel getData() {
        return data;
    }

    public void setData(QuestionsModel data) {
        this.data = data;
    }
}
