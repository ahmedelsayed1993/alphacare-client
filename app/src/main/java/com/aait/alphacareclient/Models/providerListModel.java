package com.aait.alphacareclient.Models;

import java.io.Serializable;

public class providerListModel implements Serializable {
    private int id;
    private boolean isFav;
    private String image;
    private String name;
    private String section_id;
    private String section;
    private String final_rate;
    private int count_rate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(String final_rate) {
        this.final_rate = final_rate;
    }

    public int getCount_rate() {
        return count_rate;
    }

    public void setCount_rate(int count_rate) {
        this.count_rate = count_rate;
    }
}
