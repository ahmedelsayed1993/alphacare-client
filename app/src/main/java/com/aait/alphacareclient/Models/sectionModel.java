package com.aait.alphacareclient.Models;

import java.io.Serializable;

public class sectionModel implements Serializable {
    private String  id;
    private String title;
    private boolean checked ;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public sectionModel(String id, String title) {
        this.id = id;
        this.title = title;

    }
}
