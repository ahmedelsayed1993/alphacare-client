package com.aait.alphacareclient.Gps;

public interface GpsTrakerListener {
    void onTrackerSuccess(Double lat, Double log);

    void onStartTracker();
}
