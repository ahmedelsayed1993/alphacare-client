package com.aait.alphacareclient.Network;

public class Urls {
    public final static String baseUrl = "https://alphacare.aait-sa.com/api/";

    public final static String SignUp = "user/sign-up";
    public final static String Activate = "active_code";
    public final static String Resend = "resend_code";
    public final static String LogOut = "logout";
    public final static String SignIn = "sign-in";
    public final static String FprgotPassword = "forget-password";
    public final static String NewPassword = "forget-password/update";
    public final static String ChangeLang = "change_lang";
    public final static String SiteInfo = "site-data";
    public final static String ContactTypes = "contact/types";
    public final static String SendMessage = "contact/send";
    public final static String MostQuestions = "most-ask";
    public final static String NotificationCount = "unread_notifications";
    public final static String Addresses = "user_addresses";
    public final static String AddAddress = "user_add/address";
    public final static String EditAddress = "user_edit/address";
    public final static String DeleteAddress = "user_delete/address";
    public final static String Profile = "profile";
    public final static String UpdateProfile = "profile/update";
    public final static String MainPage = "main-page";
    public final static String providerList = "providers-list";
    public final static String ShowProvider = "show-provider";
    public final static String ToogleFav = "toogle-fav";
    public final static String Sections = "sections";
    public final static String Fav = "get-fav";
    public final static String ServiceDetails = "show-product";
    public final static String Rate = "rate";
    public final static String AddOrder = "new-order";
    public final static String NewOrders = "new-orders";
    public final static String CancelOrder = "cancel-order";
    public final static String CurrentOrders = "current-orders";
    public final static String FinishedOrders = "finished-orders";
    public final static String Notification = "notifications";
    public final static String OrderDetails = "show-order";
    public final static String EditOrder = "edit-order";
    public final static String Send = "send-message";
    public final static String Chat = "room-messages";
    public final static String Status = "notification_status";
    public final static String Delete = "delete-notification";


}
