package com.aait.alphacareclient.Network;

import com.aait.alphacareclient.Models.AddressesResponse;
import com.aait.alphacareclient.Models.BaseResponse;
import com.aait.alphacareclient.Models.ChatResponse;
import com.aait.alphacareclient.Models.HomeResponse;
import com.aait.alphacareclient.Models.ListResonse;
import com.aait.alphacareclient.Models.MessageResponse;
import com.aait.alphacareclient.Models.NotificationCountRespons;
import com.aait.alphacareclient.Models.NotificationResponse;
import com.aait.alphacareclient.Models.OrderDetailsResponse;
import com.aait.alphacareclient.Models.OrderResponse;
import com.aait.alphacareclient.Models.OrdersResponse;
import com.aait.alphacareclient.Models.ProviderDetailsResponse;
import com.aait.alphacareclient.Models.ProviderListResponse;
import com.aait.alphacareclient.Models.QuestionsResponse;
import com.aait.alphacareclient.Models.SectionResponse;
import com.aait.alphacareclient.Models.ServiceResponse;
import com.aait.alphacareclient.Models.SiteInfoResponse;
import com.aait.alphacareclient.Models.ToggleResponse;
import com.aait.alphacareclient.Models.UserModel;
import com.aait.alphacareclient.Models.UserResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ServiceApi {

    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> signUp(@Query("lang") String lang,
                              @Part MultipartBody.Part avatar,
                              @Query("name") String name,
                              @Query("phone") String phone,
                              @Query("email") String email,
                              @Query("gender") String gender,
                              @Query("password") String password,
                              @Query("app_type") int app_type,
                              @Query("device_id") String device_id,
                              @Query("device_type") String device_type);

    @POST(Urls.SignUp)
    Call<UserResponse> signUpwithout(@Query("lang") String lang,
                              @Query("name") String name,
                              @Query("phone") String phone,
                              @Query("email") String email,
                              @Query("gender") String gender,
                              @Query("password") String password,
                              @Query("app_type") int app_type,
                              @Query("device_id") String device_id,
                              @Query("device_type") String device_type);

    @POST(Urls.Activate)
    Call<UserResponse> activate(@Query("lang") String lang,
                                @Query("user_id") int user_id,
                                @Query("code") String code);
    @POST(Urls.Resend)
    Call<UserResponse> resend(@Query("lang") String lang,
                              @Query("user_id") int user_id);
    @POST(Urls.LogOut)
    Call<BaseResponse> LogOut(@Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id);
    @POST(Urls.SignIn)
    Call<UserResponse> Login(@Query("lang") String lang,
                             @Query("phoneOrEmail") String phoneOrEmail,
                             @Query("password") String password,
                             @Query("app_type") int app_type,
                             @Query("device_id") String device_id,
                             @Query("device_type") String device_type);
    @POST(Urls.FprgotPassword)
    Call<UserResponse> forgotPass(@Query("lang") String lang,
                                  @Query("phone") String phone);
    @POST(Urls.NewPassword)
    Call<UserResponse> newPass(@Query("lang") String lang,
                               @Query("code") String code,
                               @Query("new_password") String new_password,
                               @Query("phone") String phone);
    @POST(Urls.ChangeLang)
    Call<BaseResponse> changeLang(@Query("app_type") int app_type,
                                  @Query("token_id") String token_id,
                                  @Query("device_id") String device_id,
                                  @Query("lang") String lang);

    @POST(Urls.SiteInfo)
    Call<SiteInfoResponse> getInfo(@Query("lang") String lang);

    @POST(Urls.ContactTypes)
    Call<ListResonse> getTypes(@Query("lang") String lang);
    @POST(Urls.SendMessage)
    Call<BaseResponse> sendMessage(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("type") int type,
                                   @Query("message") String message);

    @POST(Urls.MostQuestions)
    Call<QuestionsResponse> getQuestions(@Query("lang") String lang,
                                         @Query("page") int page);
    @POST(Urls.NotificationCount)
    Call<NotificationCountRespons> getCount(@Query("lang") String lang,
                                            @Query("app_type") int app_type,
                                            @Query("token_id") String token_id,
                                            @Query("device_id") String device_id);
    @POST(Urls.Addresses)
    Call<AddressesResponse> getAddress(@Query("lang") String lang,
                                       @Query("token_id") String token_id,
                                       @Query("device_id") String device_id,
                                       @Query("app_type") int app_type);

    @POST(Urls.AddAddress)
    Call<AddressesResponse> addAddrress(@Query("lang") String lang,
                                        @Query("app_type") int app_type,
                                        @Query("token_id") String token_id,
                                        @Query("device_id") String device_id,
                                        @Query("name") String name,
                                        @Query("lat") String lat,
                                        @Query("lng") String lng,
                                        @Query("latlng_address_ar") String latlng_address_ar,
                                        @Query("latlng_address_en") String latlng_address_en);
    @POST(Urls.EditAddress)
    Call<AddressesResponse> editAddrress(@Query("lang") String lang,
                                        @Query("app_type") int app_type,
                                        @Query("token_id") String token_id,
                                        @Query("device_id") String device_id,
                                        @Query("name") String name,
                                        @Query("lat") String lat,
                                        @Query("lng") String lng,
                                        @Query("address_id") int address_id,
                                        @Query("latlng_address_ar") String latlng_address_ar,
                                        @Query("latlng_address_en") String latlng_address_en);
    @POST(Urls.Profile)
    Call<UserResponse> getProfile(@Query("lang") String lang,
                                  @Query("token_id") String token_id,
                                  @Query("device_id") String device_id,
                                  @Query("app_type") int app_type);
    @POST(Urls.DeleteAddress)
    Call<AddressesResponse> deleteAddress(@Query("lang") String lang,
                                          @Query("app_type") int app_type,
                                          @Query("token_id") String token_id,
                                          @Query("device_id") String device_id,
                                          @Query("address_id") int address_id);

    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateWithoutPassAvatar(@Query("lang") String lang,
                                               @Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("name") String name,
                                               @Query("phone") String phone,
                                               @Query("email") String email,
                                               @Query("gender") int gender);



    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateWithoutAvatar(@Query("lang") String lang,
                                               @Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("name") String name,
                                               @Query("phone") String phone,
                                               @Query("email") String email,
                                               @Query("gender") int gender,
                                           @Query("old_password") String old_password,
                                           @Query("password") String password);
    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> update(@Query("lang") String lang,
                                           @Query("app_type") int app_type,
                                           @Query("token_id") String token_id,
                                           @Query("device_id") String device_id,
                                           @Part MultipartBody.Part avatar);

    @POST(Urls.MainPage)
    Call<HomeResponse> getHome(@Query("lang") String lang,
                               @Query("app_type") int app_type,
                               @Query("token_id") String token_id,
                               @Query("device_id") String device_id,
                               @Query("name") String name,
                               @Query("section_id") String  section_id,
                               @Query("provider_type") String provider_type,
                               @Query("gender") String gender,
                               @Query("experience") String experience,
                               @Query("rate") String rate,
                               @Query("current_lat") String current_lat,
                               @Query("current_lng") String current_lng);
    @POST(Urls.providerList)
    Call<ProviderListResponse> getList(@Query("lang") String lang,
                                       @Query("app_type") int app_type,
                                       @Query("token_id") String token_id,
                                       @Query("device_id") String device_id,
                                       @Query("name") String name,
                                       @Query("section_id") String  section_id,
                                       @Query("provider_type") String provider_type,
                                       @Query("gender") String gender,
                                       @Query("experience") String experience,
                                       @Query("rate") String rate);

    @POST(Urls.ShowProvider)
    Call<ProviderDetailsResponse> getProvider(@Query("lang") String lang,
                                              @Query("app_type") int app_type,
                                              @Query("token_id") String token_id,
                                              @Query("device_id") String device_id,
                                              @Query("provider_id") String provider_id);

    @POST(Urls.ToogleFav)
    Call<ToggleResponse> toogleFav(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("provider_id") int provider_id);

    @POST(Urls.Sections)
    Call<SectionResponse> getSections(@Query("lang") String lang);
    @POST(Urls.Fav)
    Call<UserModel.FavResponse> getFav(@Query("lang") String lang,
                                       @Query("app_type") int app_type,
                                       @Query("token_id") String token_id,
                                       @Query("device_id") String device_id);

    @POST(Urls.ServiceDetails)
    Call<ServiceResponse> getService(@Query("lang") String lang,
                                     @Query("product_id") String product_id);

    @POST(Urls.Rate)
    Call<BaseResponse> rate(@Query("lang") String lang,
                            @Query("app_type") int app_type,
                            @Query("token_id") String token_id,
                            @Query("device_id") String device_id,
                            @Query("rate") int rate,
                            @Query("comment") String comment,
                            @Query("rated_id") int rated_id,
                            @Query("order_id") String order_id);
    @POST(Urls.AddOrder)
    Call<UserResponse> AddOrder(@Query("lang") String lang,
                                @Query("app_type") int app_type,
                                @Query("token_id") String token_id,
                                @Query("device_id") String device_id,
                                @Query("provider_id") int provider_id,
                                @Query("user_address_id") int user_address_id,
                                @Query("service_ids") String service_ids,
                                @Query("lat") String lat,
                                @Query("lng") String lng,
                                @Query("latlng_address_ar") String latlng_address_ar,
                                @Query("latlng_address_en") String latlng_address_en,
                                @Query("date") String date,
                                @Query("time") String time,
                                @Query("is_blood_prob") int is_blood_prob,
                                @Query("blood_prob_desc") String blood_prob_desc);
    @POST(Urls.NewOrders)
    Call<OrdersResponse> getNew(@Query("lang") String lang,
                                @Query("app_type") int app_type,
                                @Query("token_id") String token_id,
                                @Query("device_id") String device_id,
                                @Query("page") int page);
    @POST(Urls.CurrentOrders)
    Call<OrdersResponse> getCurrent(@Query("lang") String lang,
                                @Query("app_type") int app_type,
                                @Query("token_id") String token_id,
                                @Query("device_id") String device_id,
                                @Query("page") int page);
    @POST(Urls.FinishedOrders)
    Call<OrdersResponse> getFinished(@Query("lang") String lang,
                                    @Query("app_type") int app_type,
                                    @Query("token_id") String token_id,
                                    @Query("device_id") String device_id,
                                    @Query("page") int page);
    @POST(Urls.CancelOrder)
    Call<BaseResponse> cancelOrder(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("order_id") int order_id);
    @POST(Urls.Notification)
    Call<NotificationResponse> getNotification(@Query("lang") String lang,
                                           @Query("app_type") int app_type,
                                           @Query("token_id") String token_id,
                                           @Query("device_id") String device_id,
                                           @Query("page") int page);
    @POST(Urls.OrderDetails)
    Call<OrderDetailsResponse> getOrder(@Query("lang") String lang,
                                        @Query("app_type") int app_type,
                                        @Query("token_id") String token_id,
                                        @Query("device_id") String device_id,
                                        @Query("order_id") String order_id);
    @POST(Urls.EditOrder)
    Call<UserResponse> editOrder(@Query("lang") String lang,
                                 @Query("app_type") int app_type,
                                 @Query("token_id") String token_id,
                                 @Query("device_id") String device_id,
                                 @Query("provider_id") int provider_id,
                                 @Query("order_id") String order_id,
                                 @Query("service_ids") String service_ids,
                                 @Query("lat") String lat,
                                 @Query("lng") String lng,
                                 @Query("latlng_address_ar") String latlng_address_ar,
                                 @Query("latlng_address_en") String latlng_address_en,
                                 @Query("date") String date,
                                 @Query("time") String time);
    @POST(Urls.Send)
    Call<MessageResponse> Send(@Query("app_type") int app_type,
                               @Query("token_id") String token_id,
                               @Query("device_id") String device_id,
                               @Query("room_id") String room_id,
                               @Query("message") String message);

    @POST(Urls.Chat)
    Call<ChatResponse> getChat(@Query("app_type") int app_type,
                               @Query("token_id") String token_id,
                               @Query("device_id") String device_id,
                               @Query("room_id") String room_id,
                               @Query("lang") String lang,
                               @Query("page") int page);
    @POST(Urls.Status)
    Call<UserResponse> status(@Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Query("status") int status,
                              @Query("lang") String lang);
    @POST(Urls.Delete)
    Call<BaseResponse> remove(@Query("app_type") int app_type,
                              @Query("lang") String lang,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Query("notification_id") int notification_id);

}
